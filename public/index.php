<?php

require('../vendor/autoload.php');

use Meccano\Cms;

$env = require __DIR__ .'/../config/env.php';
define('DEBUG', true);

/**
 * - environment: prod, dev, test
 * - project root dir
 * - is debugging
 */
$cms      = new Cms($env, realpath(__DIR__ .'/../'));
$response = $cms->run();
$response->send();
