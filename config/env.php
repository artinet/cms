<?php

if(!empty($_SERVER['APPLICATION_ENV'])) {
    $env = $_SERVER['APPLICATION_ENV'];
} elseif (!empty($_ENV['environment'])) {
    $env = $_ENV['environment'];
} elseif (!empty($_ENV['env'])) {
    $env = $_ENV['env'];
} elseif (!empty($_ENV['e'])) {
    $env = $_ENV['e'];
} elseif (file_exists(__DIR__ .'/../.env')) {
    $env = file_get_contents(__DIR__ .'/../.env');
} else {
    $env = 'prod';
}

return $env;