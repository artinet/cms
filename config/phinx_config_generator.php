<?php

$config = new \Meccano\Config(__DIR__, $env);

$phinxConfig = array(
    'paths' => array(
        'migrations' => realpath(__DIR__ .'/..'. $config->get('database.phinx.migrations')),
        'seeds' => realpath(__DIR__ .'/..'. $config->get('database.phinx.seeds'))
    ),
    'environments' => array(
        'default_migration_table' => 'phinxlog',
        'default_database'        => $config->getEnvironment()
    )
);

$connectionConfig = $config->get('database.connections.'. $config->get('database.defaultConnection'));
$phinxConfig['environments'][$config->getEnvironment()] = array(
    'adapter'      => $connectionConfig['adapter'],
    'host'         => $connectionConfig['hostname'],
    'name'         => $connectionConfig['database'],
    'user'         => $connectionConfig['username'],
    'pass'         => $connectionConfig['password'],
    'port'         => $connectionConfig['port'],
    'charset'      => $connectionConfig['charset'],
    'table_prefix' => $connectionConfig['table_prefix']
);

return $phinxConfig;