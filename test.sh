#!/bin/sh

# Standards: MySource, Squiz, PHPCS, PEAR, PSR1, Zend and PSR2
echo "$1"
php ./vendor/bin/phpcbf --standard=PSR2 src "$1"
php ./vendor/bin/phpcs -p --standard=PSR2 src "$1"

php ./vendor/bin/codecept run --coverage --coverage-text "$1"
