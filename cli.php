#!/usr/bin/env php
<?php

require __DIR__ .'/vendor/autoload.php';

use Meccano\Cms;
use Meccano\Commands\Cli;

set_time_limit(0);

if($argc > 2) {
    $arguments = array_slice($argv, 2);
    for($i=0; $i<sizeof($arguments); $i=$i+2) {
        $k = $arguments[$i];
        $v = isset($arguments[($i+1)]) ? $arguments[($i+1)] : false;

        $k = str_replace('-', '', $k);
        $_ENV[$k] = $v;
    }
}

$env = require __DIR__ .'/config/env.php';


$cms = new Cms($env, __DIR__, true);
$app = new Cli($cms);
$app->batchLoad();
$app->run();
