DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(3) NOT NULL,
  `title` varchar(15) NOT NULL,
  `date_format` varchar(10) NOT NULL,
  `time_format` varchar(10) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL,
  `domain` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `languages` VALUES (1,'ua','укр','d m-local-','H:i',1,0,1,NULL),(2,'en','eng','m/d/Y','g:i a',1,1,2,NULL);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(15) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_i18n` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `description_i18n` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL DEFAULT '1',
  `lt` int(11) NOT NULL,
  `rt` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `items_on_page` int(11) NOT NULL DEFAULT '10',
  `cache_on` tinyint(1) NOT NULL DEFAULT '1',
  `cache_life` int(11) NOT NULL DEFAULT '3600',
  `alias` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `forward_in` tinyint(1) NOT NULL DEFAULT '0',
  `images` text,
  PRIMARY KEY (`id`),
  KEY `lt_rt` (`lt`,`rt`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `sections_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1104 DEFAULT CHARSET=utf8;

INSERT INTO `sections` VALUES (1,1,1,8,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'admin',1,0,NULL),(2,2,2,5,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'setup',1,0,NULL),(3,3,3,4,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'languages',1,0,NULL),(4,2,6,7,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'content',1,0,NULL),(1000,1,9,24,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'index',1,0,NULL),(1001,2,10,11,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'login',1,0,NULL),(1002,2,12,13,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'register',1,0,NULL),(1003,2,14,15,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'logout',1,0,NULL),(1100,2,16,17,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'news',1,0,NULL),(1101,2,18,23,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'gallery',1,0,NULL),(1102,3,19,20,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'personal',1,0,NULL),(1103,3,21,22,'2016-06-14 12:14:33',NULL,'2016-06-14 12:14:33',NULL,10,1,3600,'funny',1,0,NULL);

DROP TABLE IF EXISTS `sections_i18n`;
CREATE TABLE `sections_i18n` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL,
  `description` text,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` text,
  PRIMARY KEY (`section_id`,`language_id`),
  UNIQUE KEY `section_language` (`section_id`,`language_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `sections_i18n_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `_sections_i18n_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sections_i18n` VALUES (1,1,'Система',NULL,NULL,NULL,NULL),(1,2,'System',NULL,NULL,NULL,NULL),(2,1,'Налаштування',NULL,NULL,NULL,NULL),(2,2,'Settings',NULL,NULL,NULL,NULL),(3,1,'Мова',NULL,NULL,NULL,NULL),(3,2,'Languages',NULL,NULL,NULL,NULL),(4,1,'Контент',NULL,NULL,NULL,NULL),(4,2,'Content',NULL,NULL,NULL,NULL),(1000,1,'Головна сторінка',NULL,NULL,NULL,NULL),(1000,2,'Main Page',NULL,NULL,NULL,NULL),(1001,1,'Авторизація',NULL,NULL,NULL,NULL),(1001,2,'Log-in',NULL,NULL,NULL,NULL),(1002,1,'Реєстрація',NULL,NULL,NULL,NULL),(1002,2,'Registration',NULL,NULL,NULL,NULL),(1003,1,'Вийти',NULL,NULL,NULL,NULL),(1003,2,'Log-out',NULL,NULL,NULL,NULL),(1100,1,'Новини',NULL,NULL,NULL,NULL),(1100,2,'News',NULL,NULL,NULL,NULL),(1101,1,'Фотографії',NULL,NULL,NULL,NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `logged_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid_to` timestamp NULL DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `image` text,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `created_by` (`created_by`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `users_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;