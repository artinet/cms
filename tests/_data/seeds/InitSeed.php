<?php

use Phinx\Seed\AbstractSeed;

class InitSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        // Set Languages
        $data = array(
            array(
                'title' => 'first one',
                'username' => 'admin',
            ),
            array(
                'title' => 'second one',
                'username' => 'qa',
            )
        );

        $test = $this->table('unit_test');
        $test
            ->insert($data)
            ->save();
    }
}
