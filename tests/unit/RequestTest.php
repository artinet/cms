<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;
use Meccano\Http\Request;

class RequestTest extends Test
{
    use \Codeception\Specify;

    public function _after()
    {
    }

    public function _before()
    {
    }

    public function testRequest()
    {
        $this->specify('Test Working path of Request', function() {

            $request = Fixtures::get('request');

            $this->assertEquals(array('en', 'hello-world'), $request->getWorkingPath());

            $request->excludeFromPath('en');
            $this->assertEquals(array('hello-world'), $request->getWorkingPath());

            $language = new \Meccano\Database\Model\Language();
            $language->fromArray(array(
                'alias' => 'ua',
                'title' => 'Ukrainian'
            ));

            $request->setCurrentLanguage($language);
            $this->assertAttributeEquals(
                $language,
                'currentLanguage',
                $request,
                'setter of current language failed'
            );
            $this->assertEquals(
                $language,
                $request->getCurrentLanguage(),
                'getter of current language failed'
            );
            $this->assertEquals(
                'ua',
                $request->getLocale(),
                'getter of locale failed'
            );

            $request->setDefaultLanguage($language);
            $this->assertAttributeEquals(
                $language,
                'defaultLanguage',
                $request,
                'setter of default language failed'
            );
            $this->assertEquals(
                $language,
                $request->getDefaultLanguage(),
                'getter of default language failed'
            );
            $this->assertEquals(
                'ua',
                $request->getDefaultLocale(),
                'getter of default locale failed'
            );
            $this->assertEquals(
                Request::CONTENT_EXT_HTML,
                $request->getAcceptableType()
            );
            
            $jsonRequest = Request::create('/news.json', 'GET');
            $this->assertEquals(
                Request::CONTENT_EXT_JSON,
                $jsonRequest->getAcceptableType()
            );
        });
    }
}