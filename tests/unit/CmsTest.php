<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;


class CmsTest extends Test
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;


    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testGetEnvironment() {
        $this->specify('Get Environment', function() {
            $this->assertEquals(
                Fixtures::get('environment'),
                Fixtures::get('cms')->getEnvironment()
            );
        });
        $this->specify('Get App name', function() {
            $this->assertEquals(
                Fixtures::get('app'),
                Fixtures::get('cms')->getApp()
            );
        });
    }

    public function testIsDebugging() {
        $this->specify('Get Debugging state', function() {
            $this->assertEquals(
                Fixtures::get('isDebug'),
                Fixtures::get('cms')->isDebug()
            );
        });
    }

    public function testDirs() {
        $this->specify('Get Config dirs', function() {
            $this->assertArraySubset(
                array(Fixtures::get('projectRoot') .'/config'),
                Fixtures::get('cms')->getConfigDirs()
            );
        });

        $this->specify('Get WebRoot dir', function() {
            $this->assertEquals(
                Fixtures::get('projectRoot') .'/public',
                Fixtures::get('cms')->getWebRoot()
            );
        });

        $this->specify('Get Log dir', function() {
            $this->assertEquals(
                Fixtures::get('projectRoot') .'/var/log',
                Fixtures::get('cms')->getLogDir()
            );
        });

        $this->specify('Get Cache dir', function() {
            $this->assertEquals(
                Fixtures::get('projectRoot') .'/var/cache',
                Fixtures::get('cms')->getCacheDir()
            );
        });
    }

    public function testGetContainer() {
        $this->specify('Get Container', function() {
            $this->assertInstanceOf(
                '\Symfony\Component\DependencyInjection\ContainerBuilder',
                Fixtures::get('cms')->getContainer()
            );
        });
    }

    public function testGetConfig() {
        $this->specify('Get Config', function() {
            $this->assertInstanceOf(
                'Meccano\Config',
                Fixtures::get('cms')->getConfig()
            );
        });
    }

    public function testGetEventDispatcher() {
        $this->specify('Get Event Dispatcher', function() {
            $this->assertInstanceOf(
                '\Symfony\Component\EventDispatcher\EventDispatcher',
                Fixtures::get('cms')->getEventDispatcher()
            );
        });
    }
}