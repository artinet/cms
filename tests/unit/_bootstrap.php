<?php

use Codeception\Util\Fixtures;

Fixtures::add('projectRoot', realpath(__DIR__ .'/../../'));
Fixtures::add('testRoot',    realpath(__DIR__ .'/../'));
Fixtures::add('app',         'front');
Fixtures::add('environment', 'test');
Fixtures::add('isDebug',     false);
Fixtures::add(
    'cms',
    new \Meccano\Cms(
        Fixtures::get('app'),
        Fixtures::get('environment'),
        Fixtures::get('projectRoot'),
        Fixtures::get('isDebug')
    )
);
Fixtures::add(
    'request',
    \Meccano\Http\Request::create('/en/hello-world', 'GET')
);