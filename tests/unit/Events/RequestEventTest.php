<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;

class RequestEventTest extends Test
{
    use \Codeception\Specify;

    protected function _before()
    {

    }

    protected function _after()
    {

    }

    public function testEvent() {
        $this->specify('Test Request Event getters', function() {
            $event = new \Meccano\Events\RequestEvent(
                Fixtures::get('cms')->getContainer(),
                new \Meccano\Http\Request()
            );

            $this->assertInstanceOf(
                '\Meccano\Http\Request',
                $event->getRequest(),
                "can't get Request (". get_class($event->getRequest()) .')'
            );
            $this->assertInstanceOf(
                '\Symfony\Component\DependencyInjection\ContainerInterface',
                $event->getContainer(),
                "can't get Container (". get_class($event->getContainer()) .")"
            );
        });
    }
}
