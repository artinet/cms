<?php

use Codeception\TestCase\Test;

class DataCollectionTest extends Test
{
    use \Codeception\Specify;

    public function testDataCollection()
    {
        $this->specify('Test DataCollection class', function() {
            $data = new \Meccano\Database\DataCollection();
            $el_0 = new stdClass();
            $el_0->title = 'hello world';
            $el_1 = new stdClass();
            $el_1->title = 'buy the world';
            $el_2 = new stdClass();

            $data->add($el_0);
            $data->add($el_1);
            $data->add($el_2);
            $this->assertEquals($el_0, $data->first());
            $this->assertEquals(3, $data->count());
            $this->assertEquals($el_1, $data->get(1));

            $data->remove(1);
            $data->removeByItem($el_2);
            $this->assertEquals(1, $data->count());
            $this->assertNull($data->get(1));
            $this->assertNull($data->get(2));

            $data->set(4, $el_1);
            $this->assertEquals($el_1, $data->get(4));
            $this->assertEquals($el_1, $data->find(function($i, $item) {
                if($item->title == 'buy the world') {
                    return true;
                }
                return false;
            }));
        });
    }
}