<?php

use Codeception\Util\Fixtures;

class LanguageRepositoryTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;

    public function _before()
    {
    }

    public function _after()
    {
    }

    public function testDefaultLanguage()
    {
        $this->specify('Test Default Language', function() {
            /** @var \Meccano\Database\Db $db */
            $db = Fixtures::get('cms')->getContainer()->get('database');
            /** @var \Meccano\Database\Repository\LanguageRepository $languageRepository */
            $languageRepository = $db->model('Language');

            $this->assertInstanceOf('\Meccano\Database\Model\Language', $languageRepository->getDefaultLanguage());
            $this->assertEquals(1, $languageRepository->getDefaultLanguage()['is_default'], 'The `is_default` property contains wrong data *** - '. $languageRepository->getDefaultLanguage()['is_default']);
        });
    }
}