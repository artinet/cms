<?php

use Codeception\Util\Fixtures;

class ModelRepositoryTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;

    public function _before()
    {
    }

    public function _after()
    {
    }

    public function testModelRepository()
    {
        $this->specify('Test Model Repository', function() {
            /** @var \Meccano\Database\Db $db */
            $db = Fixtures::get('cms')->getContainer()->get('database');

            $model = new \Meccano\Database\Model\Language();

            $modelRepository = new \Meccano\Database\Repository\ModelRepository();

            $modelRepository->setConnection($db);
            $this->assertAttributeInstanceOf('\Meccano\Database\Db', 'db', $modelRepository);

            $modelRepository->setModel($model);
            $this->assertAttributeInstanceOf('\Meccano\Database\Model\ModelInterface', 'model', $modelRepository);

            /** @var \Meccano\Database\DataCollection $data */
            $data = $modelRepository->all();

            $this->assertInstanceOf('\Meccano\Database\DataCollection', $data);
            $this->assertCount(2, $data, 'COUNT: '. $data->count());
        });
    }

    public function testRepositoryORM()
    {
        /** @var Meccano\Database\Db $db */
        $db = Fixtures::get('cms')->getContainer()->get('database');

        $model = new \Meccano\Database\Model\Language();

        /** @var \Meccano\Database\Repository\ModelRepository $modelRepository */
        $modelRepository = new \Meccano\Database\Repository\ModelRepository();
        $modelRepository->setConnection($db);
        $modelRepository->setModel($model);
        $modelRepository->reset();

        $this->specify('Test ORM setters', function() use ($modelRepository) {

            $modelRepository->limit(1);
            $this->assertAttributeEquals(1, 'rowsLimit', $modelRepository, 'LIMIT setter failed');

            $modelRepository->offset(5);
            $this->assertAttributeEquals(5, 'rowsOffset', $modelRepository, 'OFFSET setter failed');

            $modelRepository->order('alias', 'desc');
            $this->assertAttributeEquals(array('alias' => '`alias` desc'), 'rowsOrder', $modelRepository, 'ORDER BY setter failed');

            $modelRepository->addOrder('id');
            $this->assertAttributeEquals(array('alias' => '`alias` desc', 'id' => '`id` asc'), 'rowsOrder', $modelRepository, 'add ORDER BY setter failed');

            $modelRepository->reset();
            $this->assertAttributeEmpty('rowsLimit', $modelRepository, '->reset() failed: rowsLimit');
            $this->assertAttributeEmpty('rowsOffset', $modelRepository, '->reset() failed: rowsOffset');
            $this->assertAttributeEmpty('rowsOrder', $modelRepository, '->reset() failed: rowsOrder');
            $this->assertAttributeCount(0, 'allData', $modelRepository, '->reset() failed: allData');
            $this->assertAttributeCount(0, 'conditions', $modelRepository, '->reset() failed: conditions');
        });

        $this->specify('Test ORM getters', function() use ($modelRepository) {

            $data = $modelRepository->reset()
                ->limit(2)
                ->get();
            $this->assertCount(2, $data, '->get() failed');

            $data = $modelRepository->where('alias', 'en', '=')
                ->getOne();
            $this->assertInstanceOf('\Meccano\Database\Model\ModelInterface', $data, '->getOne() failed: returned wrong object: '. get_class($data));
            $this->assertEquals('en', $data->alias, '->getOne() failed: returned wrong data');

            $data = $modelRepository->where('id', 0, '>')
                ->andWhere('alias', 'en', '=')
                ->get();
            $this->assertCount(1, $data, '->andWhere() failed');

            $data = $modelRepository->where('id', 0, '<')
                ->orWhere('alias', 'en', '=')
                ->get();
            $this->assertCount(1, $data, '->orWhere() failed');

            $data = $modelRepository->find(1);
            $this->assertInstanceOf('\Meccano\Database\Model\ModelInterface', $data, '->find() failed: returned wrong object: '. get_class($data));
        });

        $this->specify('Test Direct query', function() use ($modelRepository) {

            $data = $modelRepository->query('SELECT id FROM languages WHERE id > ? ORDER BY id ASC LIMIT 1', array(0));

            $this->assertEquals(array(0 => array('id' => 1)), $data, '->query() failed');
        });
    }
}