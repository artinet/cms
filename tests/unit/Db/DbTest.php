<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;
use Meccano\Database\Db;
use Meccano\Database\DbFactory;
use Meccano\Config;

class DbTest extends Test
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  Db */
    private $db;

    protected function _before()
    {
        $dbModule = $this->getModule('Db');
        $this->db = new Db(
            $dbModule->_getConfig('dsn'),
            $dbModule->_getConfig('user'),
            $dbModule->_getConfig('password')
        );
    }

    protected function _after()
    {
    }

    public function testConnection()
    {
        $this->specify('Check DB Connection', function() {
            $this->assertInstanceOf('PDO', $this->db->getConnection());
        });
    }

    public function testDbFactory()
    {
        // TODO: point to the independent DB config
        $configProd = new Config(Fixtures::get('projectRoot') .'/config', 'test');

        $this->specify('Check DB Factory', function() use ($configProd) {
            $this->assertInstanceOf('Meccano\Database\Db', DbFactory::createDbService($configProd));
        });
    }

    public function testSettersGetters()
    {
        $this->specify('Test Get/Set table prefix', function() {
            $this->db->setTablePrefix('test_prefix_');
            $this->assertEquals('test_prefix_', $this->db->getTablePrefix());
        });
    }
}