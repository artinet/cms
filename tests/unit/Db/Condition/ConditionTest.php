<?php

class ConditionTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;

    public function _before()
    {
    }

    public function _after()
    {
    }

    public function testCondition()
    {
        $this->specify('Test Condition', function() {
            $condition = new \Meccano\Database\Condition\Condition();

            $condition
                ->setField('title')
                ->setCondition('=')
                ->setValue('22');

            $this->assertEquals('`title` = ?', $condition->getSql());
            $this->assertEquals(array('22'), $condition->getParameters());
        });
    }
}