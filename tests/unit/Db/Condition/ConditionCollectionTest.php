<?php

class ConditionCollectionTest extends \Codeception\TestCase\Test
{
    use \Codeception\Specify;

    public function _before()
    {
    }

    public function _after()
    {
    }

    public function testSQL()
    {
        $this->specify('Test ConditionCollection to get SQL and parameters', function() {
            $conditionCollection = new \Meccano\Database\Condition\ConditionCollection();

            $conditionOne = new \Meccano\Database\Condition\Condition();
            $conditionOne
                ->setField('title')
                ->setCondition('>')
                ->setValue('one');
            $conditionCollection->addAndCondition($conditionOne);

            $conditionTwo = new \Meccano\Database\Condition\Condition();
            $conditionTwo
                ->setField('f')
                ->setCondition('=')
                ->setValue(1);
            $conditionCollection->addOrCondition($conditionTwo);


            $this->assertEquals('`title` > ? or `f` = ?', $conditionCollection->getSql());
            $this->assertEquals(array('one', 1), $conditionCollection->getParameters());

            $nestedConditions = new \Meccano\Database\Condition\ConditionCollection();
            $nestedConditions->addAndCondition($conditionOne);
            $nestedConditions->addOrCondition($conditionTwo);
            $conditionCollection->addAndCondition($nestedConditions);

            $this->assertEquals('`title` > ? or `f` = ? and (`title` > ? or `f` = ?)', $conditionCollection->getSql());
        });
    }
}