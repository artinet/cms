<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;

class DbModelTest extends Test
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testModelLoad()
    {
        $this->specify('Load model Repository', function() {
            $db = Fixtures::get('cms')->getContainer()->get('database');

            $this->assertInstanceOf(
                '\Meccano\Database\Repository\ModelRepository',
                $db->model('Language')
            );
        });
    }
}