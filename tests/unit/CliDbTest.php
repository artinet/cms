<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;
use Meccano\Commands\Cli;
use Meccano\Commands\Db\CreateMigrationCommand;
use Meccano\Commands\Db\CreateSeedCommand;
use Meccano\Commands\Db\MigrateCommand;
use Meccano\Commands\Db\RollbackCommand;
use Meccano\Commands\Db\SeedRunCommand;
use Symfony\Component\Console\Tester\CommandTester;

class CliDbTest extends Test
{
    use \Codeception\Specify;

    /** @var Cli */
    private $cli;

    protected function _before()
    {
        $this->cli = new Cli(Fixtures::get('cms'));
    }

    protected function _after()
    {
        // Remove generated migrations
        $migrationPath = realpath(Fixtures::get('projectRoot') . Fixtures::get('cms')->getConfig()->get('database.phinx.migrations'));
        chdir($migrationPath);
        foreach (glob('*_test.php') as $filename) {
            unlink($filename);
        }

        // Remove generated seeds
        $seedPath = realpath(Fixtures::get('projectRoot') . Fixtures::get('cms')->getConfig()->get('database.phinx.seeds'));
        chdir($seedPath);
        foreach (glob('*est.php') as $filename) {
            unlink($filename);
        }
    }

    public function testCreateMigration() {

        $this->specify('Test creation of DB migration', function() {
            $this->cli->add(new CreateMigrationCommand());

            $command = $this->cli->find('db:migration:create');
            $commandTester = new CommandTester($command);
            $commandTester->execute(
                array(
                    'command' => $command->getName(),
                    'name' => 'Test'
                )
            );

            $this->assertRegExp('/created/', $commandTester->getDisplay());
        });

        $this->specify('Test creation of DB seed', function() {
            $this->cli->add(new CreateSeedCommand());

            $command = $this->cli->find('db:seed:create');
            $commandTester = new CommandTester($command);
            $commandTester->execute(array(
                'command' => $command->getName(),
                'name' => 'Test'
            ));

            $this->assertRegExp('/created/', $commandTester->getDisplay());
        });
    }

    public function testMigrationUp() {
        $this->specify('Test migration up', function() {
            $this->cli->add(new MigrateCommand());

            $command = $this->cli->find('db:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute(array('command' => $command->getName()));

            $this->assertRegExp('/Init: migrated/', $commandTester->getDisplay());
        });
    }


    public function testSeed() {
        $this->specify('Test DB seeding', function() {
            $this->cli->add(new SeedRunCommand());

            $command = $this->cli->find('db:seed:run');
            $commandTester = new CommandTester($command);
            $commandTester->execute(array('command' => $command->getName()));

            $this->assertRegExp('/InitSeed: seeded/', $commandTester->getDisplay());
            $dbModule = $this->getModule('Db');
            $dbModule->seeInDatabase('unit_test', array('title' => 'second one', 'username' => 'qa'));
        });
    }

    public function testMigrationDown() {
        $this->specify('Test migration down', function() {
            $this->cli->add(new RollbackCommand());

            $command = $this->cli->find('db:rollback');
            $commandTester = new CommandTester($command);
            $commandTester->execute(array('command' => $command->getName()));

            $this->assertRegExp('/Init: reverted/', $commandTester->getDisplay());
        });
    }
}