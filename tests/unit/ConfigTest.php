<?php

use Codeception\TestCase\Test;
use Codeception\Util\Fixtures;
use Meccano\Config;

class ConfigTest extends Test
{

    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Test Config loading
     */
    public function testLoadConfig()
    {
        $testRoot = Fixtures::get('testRoot');

        // TODO: replace configs with copies of real configs from the system
        $configProd = new Config($testRoot .'/_data/config', 'prod');
        $configTest  = new Config($testRoot .'/_data/config', 'test');

        /**
         * Test Loading config
         */
        $this->specify('Load default config values', function() use ($configProd) {
            $this->assertEquals('clear value', $configProd->get('test.val'));
            $this->assertEquals('override value', $configProd->get('test.var_override'));
        });

        /**
         * Test overriding config
         */
        $this->specify('Override config values by environment', function() use ($configTest) {
            $this->assertEquals('clear value', $configTest->get('test.val'));
            $this->assertEquals('new test value', $configTest->get('test.var_override'));
        });

        /**
         * Test loading not existed config
         */
        $this->specify('Load not existed config', function() use ($configProd) {
            $this->assertNull($configProd->get('foo.var'));
        });
    }

    public function testConfigEnvironment()
    {
        $this->specify('Test Environment setter', function() {
            $testRoot = Fixtures::get('testRoot');
            $config = new Config($testRoot .'/_data/config', 'prod');

            $this->assertEquals('prod', $config->getEnvironment());
        });
    }
}