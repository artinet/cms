const path = require('path');

module.exports = {
    context: __dirname,
    mode: 'production',
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, '../public/admin/js')
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader' }
        ]
    }
};