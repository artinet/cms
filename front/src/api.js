import axios from 'axios';

class Response {
    status: number;

    constructor(res) {
        this.status = res.status;
        this.headers = res.headers;
        this.body = res.data;
        this.token = res.headers['x-meccano-token'] || null
    }
}

export class ErrorResponse extends Response {
    constructor(res) {
        super(res);
        this.error = res.statusText;
    }
}

const api = (path, method, data, headers) => {

    const settings = {
        method,
        data,
        url: '' + path,
        headers: Object.assign(
            {
                'content-type': 'application/json',
                'accept': 'application/json',
                'X-JWT-AUTH': 'True'
            },
            headers || {}
        )
    };

    return axios(settings)
        .then(res => new Response(res))
        .catch(err => {
            if ('response' in err) {
                return new ErrorResponse(err.response);
            } else {
                return new ErrorResponse({status: 500, statusText: 'Connection failed', body: null, headers: null});
            }
        })
};

export const get = (path, data, headers) => {
    return api(path, 'get', data, headers);
};

export const post = (path, data, headers) => {
    return api(path, 'post', data, headers);
};

export const put = (path, data, headers) => {
    return api(path, 'put', data, headers);
};


export default api;