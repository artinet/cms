import { all } from 'redux-saga/effects';

import { login } from './containers/login/saga';


export default function* rootSaga() {
    yield all([
        login()
    ])
}
