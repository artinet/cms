import React from 'react'
import PropTypes from 'prop-types';
import { Link, Route } from "react-router-dom";
import { Logout } from '../containers/login/component';

class Layout extends React.Component {
    constructor(props) {
        super(props);
        props.user.full_name = [props.user.first_name, props.user.last_name].join(" ");
        props.user.signature = props.user.full_name + '[' + props.user.username + ']';
    }

    toggleAside() {
        $('body').toggleClass('open');
    }

    render() {
        const { dispatch, user } = this.props

        return (
            <div className="right-panel">
                <header className="header">
                    <div className="header-menu">
                        <div className="col-sm-7">
                            <a id="menuToggle" className="menutoggle pull-left" onClick={this.toggleAside}>
                                <i className="fa fa-tasks"></i>
                            </a>
                        </div>
                        <div className="col-sm-5">
                            <div className="user-area dropdown float-right">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img className="user-avatar rounded-circle" src="/admin/img/user.png" title={user.signature} />
                                </a>
                                <div className="user-menu dropdown-menu">
                                    <a className="nav-link" href="#">
                                        <i className="fa fa-user"></i> My Profile
                                    </a>
                                    <Logout dispatch={dispatch} />
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

Layout.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default Layout;