import React from 'react';

const sectionContainer = 'sections-tree';

export class LeftPanel extends React.Component {
    componentDidMount() {
        $("#" + sectionContainer).jstree({
            types: {
                folder: {
                    icon: "jstree-folder",
                    valid_children : []
                },
                "folder-empty": {
                    icon: "jstree-folder-empty",
                    valid_children: []
                },
                "folder-empty-disabled": {
                    icon: "jstree-folder-empty-disabled",
                    valid_children: []
                },
                "folder-empty-handler": {
                    icon: "jstree-folder-empty-handler",
                    valid_children: []
                },
                "folder-disabled": {
                    icon: "jstree-folder-disabled",
                    valid_children: []
                },
                "folder-handler": {
                    icon: "jstree-folder-handler",
                    valid_children: []
                },
                article : {
                    icon : "jstree-file",
                    valid_children : []
                },
                "article-disabled": {
                    icon: 'jstree-file-disabled',
                    valid_children: []
                }
            },
            contextmenu: {
                items: node => {
                    console.log('Node: ', node);
                    let cm = {}
                    cm.create = {
                        label: "Create",
                        action: () => {
                           alert('create');
                        },
                        icon: "ti-pencil"
                    }
                    cm.remove = {
                        label: "Delete",
                        action: () => {
                            alert("delete");
                        },
                        icon: "ti-trash"
                    }

                    return cm;
                }
            },
            plugins: ["types", "contextmenu"],
            core: {
                data: [
                    { "id" : "ajson1", "parent" : "#", "text" : "Simple root node", type: "folder-empty" },
                    { "id" : "ajson2", "parent" : "#", "text" : "Simple root node", type: "folder-empty-disabled" },
                    { "id" : "ajson3", "parent" : "#", "text" : "Simple root node", type: "folder-empty-handler" },
                    { "id" : "ajson4", "parent" : "#", "text" : "Root node 2", type: "folder" },
                    { "id" : "ajson5", "parent" : "ajson4", "text" : "Child 1", type: "folder-empty" },
                    { "id" : "ajson6", "parent" : "ajson4", "text" : "Child 2", type: "article" },
                    { "id" : "ajson7", "parent" : "ajson4", "text" : "Child 3", type: "article-disabled" },
                    { "id" : "ajson8", "parent" : "#", "text" : "Another node", type: "folder-handler" },
                    { "id" : "ajson9", "parent" : "ajson8", "text" : "Child 2", type: "article" },
                    { "id" : "ajson10", "parent" : "ajson8", "text" : "Child 3", type: "article-disabled" },
                ]
            }
        });
    }

    render() {
        return (
            <aside className="left-panel">
                <nav className="navbar navbar-expand-sm navbar-default">
                    <div className="navbar-header">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                            <i className="fa fa-bars"></i>
                        </button>
                        <a className="navbar-brand" href="/admin/">
                            <img src="/admin/img/logo.png" title="Artinet" />
                        </a>
                        <a className="navbar-brand hidden" href="/admin/">
                            <img src="/admin/img/logo-sm.png" title="Artinet" />
                        </a>
                    </div>
                    <div id={sectionContainer} className="tree-view">
                    </div>
                </nav>
            </aside>
        )
    }
}