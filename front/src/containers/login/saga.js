import { put, takeLatest, call } from 'redux-saga/effects';

import * as types from '../../action_types';
import { post, ErrorResponse } from '../../api';

function* loginRequest(data) {
    try {
        const response = yield call(() => post('/login', data.payload));
        console.log('Response is: ', response, typeof(response));
        if (response instanceof ErrorResponse) {
            yield put({type: types.LOGIN.FAILED, payload: response});
        } else {
            yield put({type: types.LOGIN.SUCCESS, payload: response});
        }
    } catch (e) {
        yield put({type: types.LOGIN.FAILED, payload: e});
    }
}

/*
export function* loginRequest(data) {
    console.log('login request', data);
    //yield api('/login')
    yield put({type: types.LOGIN.SUCCESS, payload: 'loged'})
}
*/

export function* login() {
    yield takeLatest(types.LOGIN.REQUEST, loginRequest)
}
