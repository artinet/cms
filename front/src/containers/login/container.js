import { connect } from 'react-redux';
import LoginFormComponent from './component';

const mapStateToProps = state => {
    return {
        error: state.tokenReducer.error,
        username: null,
        password: null
    };
};

export const LoginFormContainer = connect(mapStateToProps)(LoginFormComponent);