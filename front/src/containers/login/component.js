import React from 'react';
import PropTypes from 'prop-types';
import { loginAction, logoutAction } from './action';

export class LoginFormComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        }
    }

    render() {
        return (
            <div className="login-page">
                <div className="login-form">
                    <form>
                        <div className="form-group">
                            <input 
                                className={"form-control" + (this.props.error ? ' is-invalid' : '')} 
                                type="text" 
                                placeholder="Login"
                                onChange={(e) => {this.setState({username: e.target.value});}} 
                            />
                        </div>
                        <div className="form-group">
                            <input 
                                className={"form-control" + (this.props.error ? ' is-invalid' : '')} 
                                type="password" 
                                placeholder="Password"
                                autoFocus 
                                onChange={(e) => {this.setState({password: e.target.value});}} 
                            />
                        </div>
                        <div className="form-group">
                            <button 
                                className="btn btn-primary btn-flat" 
                                onClick={() => { this.props.dispatch(loginAction(this.state.username, this.state.password)) }}>
                                <i className="fa fa-user"></i> Log-in
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

LoginFormComponent.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export class Logout extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
    }
    render() {
        const { dispatch } = this.props;
        return (
            <a className="nav-link" href="#" onClick={() => dispatch(logoutAction())}>
                <i className="fa fa-power-off"></i> Logout
            </a>
        )
    }
}

Logout.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default LoginFormComponent;