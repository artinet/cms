import * as types from "../../action_types";

const initialState = {
    error: false,
    token: sessionStorage.getItem('token'),
    user: sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user')) : null
};

export const tokenReducer = (state=initialState, action) => {
    switch (action.type) {
        case types.LOGIN.REQUEST:
            return {...state, ...action.payload};

        case types.LOGIN.SUCCESS:
            let response = action.payload
            sessionStorage.setItem('token', response.token)
            sessionStorage.setItem('user', JSON.stringify(response.body.data))
            return {...state, error: false, user: response.body.data, token: response.token};

        case types.LOGIN.FAILED:
            return {...state, error: true};

        case types.LOGIN.EXIT:
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('user');
            return initialState;

        default:
            return initialState;
    }
};