import * as types from '../../action_types';

export const loginAction = (username, password) => {
    return {
        type: types.LOGIN.REQUEST,
        payload: {
            _username: username,
            _password: password
        }
    }
};

export const logoutAction = () => {
    return {
        type: types.LOGIN.EXIT,
        payload: null
    }
}