import { combineReducers } from "redux";
import { tokenReducer } from "./containers/login/reducer";

export const cmsReducer = combineReducers({
    tokenReducer
});
