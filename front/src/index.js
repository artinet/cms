import React from 'react'
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Redirect } from "react-router-dom";
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { cmsReducer } from './reducer';
import rootSaga from './sagas';
import Layout from './components/layout';
import { LeftPanel } from './containers/leftPanel/component';
// import { LoginFormContainer as Login } from "./containers/login/container";
import { LoginFormComponent as Login, Logout } from "./containers/login/component"



const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    cmsReducer,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(rootSaga);

class App extends React.Component {
    render() {
        const { authenticated, dispatch, token, user, error } = this.props;
        if (authenticated) {
            return (
                <div className="app-container">
                    <LeftPanel dispatch={dispatch} />
                    <Layout dispatch={dispatch} user={user} />
                </div>
            )
        } else {
            return (
                <Login dispatch={dispatch} error={error} />
            )
        }
    }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    authenticated: PropTypes.bool.isRequired,
    token: PropTypes.string,
    user: PropTypes.object,
    error: PropTypes.bool
};

const mapStateToProps = state => {
    let istate = state.tokenReducer;
    return {
        token: istate.token || null,
        user: istate.user || null,
        error: istate.error || false,
        authenticated: istate.token && istate.user ? true : false
    };
};

const mapDispatchToProps = dispatch => {
    return { dispatch: dispatch };
};

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <AppContainer />
        </Router>
    </Provider>,
    document.getElementById('meccano-admin-app')
)