<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Init extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $languages = $this->table('languages');
        $languages
            ->addColumn('alias', 'string', array('limit' => 3))
            ->addColumn('title', 'string', array('limit' => 15))
            ->addColumn('date_format', 'string', array('limit' => 10))
            ->addColumn('time_format', 'string', array('limit' => 10))
            ->addColumn('is_active', 'boolean', array('default' => true))
            ->addColumn('is_default', 'boolean', array('default' => false))
            ->addColumn('position', 'integer', array('signed' => false))
            ->addColumn('domain', 'string', array('limit' => 50, 'null' => true))
            ->addIndex(array('alias'), array( 'unique' => true))
            ->create();

        $users = $this->table('users');
        $users
            ->addColumn('created_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('created_by', 'integer', array('null' => true))
            ->addColumn('modified_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('modified_by', 'integer', array('null' => true))
            ->addColumn('logged_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('valid_to', 'timestamp', array('default' => null, 'null' => true))
            ->addColumn('username', 'string', array('limit' => 50, 'null' => false))
            ->addColumn('pwd', 'string', array('limit' => 255))
            ->addColumn('image', 'text', array('null' => true))
            ->addColumn('first_name', 'string', array('limit' => 50))
            ->addColumn('last_name', 'string', array('limit' => 50))
            ->addColumn('is_active', 'boolean', array('default' => false))
            ->addColumn('is_deleted', 'boolean', array('default' => false))
            ->addColumn('hash', 'string', array('limit' => 32, 'null' => true))
            ->addForeignKey('created_by', 'users', 'id', array('delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'))
            ->addForeignKey('modified_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addIndex(array('username'), array( 'unique' => true))
            ->create();

        $roles = $this->table('roles');
        $roles
            ->addColumn('alias', 'string', array('limit' => 15, 'null' => false))
            ->addColumn('title', 'string', array('limit' => 50, 'null' => false))
            ->addColumn('title_i18n', 'string', array('limit' => 100))
            ->addColumn('description', 'string', array('limit' => 255))
            ->addColumn('description_i18n', 'string', array('limit' => 100))
            ->addIndex(array('alias'), array( 'unique' => true))
            ->create();

        $users2roles = $this->table('users_roles');
        $users2roles
            ->addColumn('user_id', 'integer')
            ->addColumn('role_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->addForeignKey('role_id', 'roles', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->create();

        $sections = $this->table('sections');
        $sections
            ->addColumn('level', 'integer', array('default' => 1))
            ->addColumn('lt', 'integer')
            ->addColumn('rt', 'integer')
            ->addColumn('created_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('created_by', 'integer', array('null' => true))
            ->addColumn('modified_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('modified_by', 'integer', array('null' => true))
            ->addColumn('items_on_page', 'integer', array('default' => 10))
            ->addColumn('cache_on', 'boolean', array('default' => true))
            ->addColumn('cache_life', 'integer', array('default' => 3600))
            ->addColumn('alias', 'string', array('limit' => 20))
            ->addColumn('is_active', 'boolean', array('default' => true))
            ->addColumn('forward_in', 'boolean', array('default' => false))
            ->addColumn('images', 'text', array('null' => true))
            ->addColumn('types', 'integer', array('null' => true))
            ->addIndex(array('lt', 'rt'), array('name' => 'lt_rt'))
            ->addForeignKey('created_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addForeignKey('modified_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->create();

        $sections_i18n = $this->table('sections_i18n', array('id' => false, 'primary_key' => array('section_id', 'language_id')));
        $sections_i18n
            ->addColumn('section_id', 'integer', array('null' => true))
            ->addColumn('language_id', 'integer', array('null' => true))
            ->addColumn('title', 'string', array('limit' => 200))
            ->addColumn('description', 'text', array('null' => 'true'))
            ->addColumn('meta_title', 'string', array('limit' => 200, 'null' => true))
            ->addColumn('meta_keywords', 'text', array('limit' => MysqlAdapter::TEXT_TINY))
            ->addColumn('meta_description', 'text', array('limit' => MysqlAdapter::TEXT_MEDIUM))
            ->addForeignKey('section_id', 'sections', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->addForeignKey('language_id', 'languages', 'id', array('delete' => 'NO_ACTION', 'update' => 'NO_ACTION'))
            ->addIndex(array('section_id', 'language_id'), array( 'unique' => true, 'name' => 'section_language'))
            ->create();

        $articles = $this->table('articles');
        $articles
            ->addColumn('section_id', 'integer', array('null' => true))
            ->addColumn('created_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('created_by', 'integer', array('null' => true))
            ->addColumn('modified_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('modified_by', 'integer', array('null' => true))
            ->addColumn('deleted_at', 'timestamp', array('null' => true, 'default' => null))
            ->addColumn('deleted_by', 'integer', array('null' => true))
            ->addColumn('alias', 'string', array('null' => 'true', 'limit' => 20))
            ->addColumn('position', 'integer', array('default' => 1))
            ->addColumn('is_active', 'boolean', array('default' => true))
            ->addColumn('types', 'integer', array('null' => true))
            ->addColumn('images', 'text', array('null' => true))
            ->addForeignKey('section_id', 'sections', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addForeignKey('created_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addForeignKey('modified_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addForeignKey('deleted_by', 'users', 'id', array('delete' => 'SET_NULL', 'update' => 'NO_ACTION'))
            ->addIndex(array('position'), array('name' => 'position'))
            ->addIndex(array('is_active', 'alias'), array('name' => 'is_active_alias'))
            ->addIndex(array('is_active', 'section_id'), array('name' => 'is_active_section'))
            ->create();

        $articles_i18n = $this->table('articles_i18n', array('id' => false, 'primary_key' => array('article_id', 'language_id')));
        $articles_i18n
            ->addColumn('article_id', 'integer', array('null' => true))
            ->addColumn('language_id', 'integer', array('null' => true))
            ->addColumn('title', 'string', array('limit' => 200))
            ->addColumn('sub_title', 'string', array('limit' => 500))
            ->addColumn('description', 'text', array('default' => '', 'limit' => MysqlAdapter::TEXT_MEDIUM))
            ->addColumn('body', 'text', array('default' => ''))
            ->addColumn('meta_title', 'string', array('limit' => 200))
            ->addColumn('meta_keywords', 'text', array('limit' => MysqlAdapter::TEXT_TINY))
            ->addColumn('meta_description', 'text', array('limit' => MysqlAdapter::TEXT_MEDIUM))
            ->addForeignKey('article_id', 'articles', 'id', array('delete' => 'CASCADE', 'update' => 'NO_ACTION'))
            ->addForeignKey('language_id', 'languages', 'id', array('delete' => 'NO_ACTION', 'update' => 'NO_ACTION'))
            ->create();
    }
}
