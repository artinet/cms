<?php

return [
    [
        'id' => 1,
        'alias' => 'ua',
        'title' => 'укр',
        'date_format' => 'd m-local-of, Y',
        'time_format' => 'H:i',
        'is_active' => true,
        'is_default' => false,
        'position' => 1,
    ],
    [
        'id' => 2,
        'alias' => 'en',
        'title' => 'eng',
        'date_format' => 'm/d/Y',
        'time_format' => 'g:i a',
        'is_active' => true,
        'is_default' => true,
        'position' => 2
    ]
];