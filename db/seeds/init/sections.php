<?php

return [
    [
        'id' => 1,
        'lt' => 1,
        'rt' => 2,
        'level' => 1,
        'alias' => 'admin'
    ],
    [
        'id' => 2,
        'lt' => 3,
        'rt' => 4,
        'level' => 1,
        'alias' => 'login'
    ],
    [
        'id' => 3,
        'lt' => 5,
        'rt' => 6,
        'level' => 1,
        'alias' => 'register'
    ],
    [
        'id' => 4,
        'lt' => 7,
        'rt' => 8,
        'level' => 1,
        'alias' => 'logout'
    ],
    [
        'id' => 100,
        'lt' => 9,
        'rt' => 12,
        'level' => 1,
        'alias' => 'api'
    ],
    [
        'id' => 101,
        'lt' => 10,
        'rt' => 11,
        'level' => 2,
        'alias' => 'section'
    ],
    [
        'id' => 1000,
        'lt' => 13,
        'rt' => 14,
        'level' => 1,
        'alias' => 'index'
    ],
    [
        'id' => 1100,
        'lt' => 15,
        'rt' => 16,
        'level' => 1,
        'alias' => 'news'
    ],
    [
        'id' => 1101,
        'lt' => 17,
        'rt' => 22,
        'level' => 1,
        'alias' => 'gallery'
    ],
    [
        'id' => 1102,
        'lt' => 18,
        'rt' => 19,
        'level' => 2,
        'alias' => 'personal'
    ],
    [
        'id' => 1103,
        'lt' => 20,
        'rt' => 21,
        'level' => 2,
        'alias' => 'funny'
    ]
];