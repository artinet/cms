<?php
return [
    [
        'id' => 1,
        'alias' => 'admin',
        'title' => 'Admin',
        'title_i18n' => 'role.admin',
        'description' => 'Full access to admin panel'
    ],
    [
        'id' => 2,
        'alias' => 'editor',
        'title' => 'Editor',
        'title_i18n' => 'role.editor',
        'description' => 'Limited access to admin panel. User is allowed to edit content.'
    ],
    [
        'id' => 3,
        'alias' => 'user',
        'title' => 'Registered user',
        'title_i18n' => 'role.user',
        'description' => 'Regular registered user, access rights depends to front-end development'
    ],
    [
        'id' => 4,
        'alias' => 'subscriber',
        'title' => 'E-mail subscriber',
        'title_i18n' => 'role.subscriber',
        'description' => 'User subscribed to e-mail dispatch'
    ]
];