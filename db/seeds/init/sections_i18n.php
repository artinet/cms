<?php
return [
    [
        'section_id' => 1,
        'language_id' => 1,
        'title' => 'Система'
    ],
    [
        'section_id' => 1,
        'language_id' => 2,
        'title' => 'System'
    ],
    [
        'section_id' => 2,
        'language_id' => 1,
        'title' => 'Авторизація'
    ],
    [
        'section_id' => 2,
        'language_id' => 2,
        'title' => 'Log-in'
    ],
    [
        'section_id' => 3,
        'language_id' => 1,
        'title' => 'Реєстрація'
    ],
    [
        'section_id' => 3,
        'language_id' => 2,
        'title' => 'Registration'
    ],
    [
        'section_id' => 4,
        'language_id' => 1,
        'title' => 'Вийти'
    ],
    [
        'section_id' => 4,
        'language_id' => 2,
        'title' => 'Log-out'
    ],
    [
        'section_id' => 100,
        'language_id' => 1,
        'title' => 'АПІ'
    ],
    [
        'section_id' => 100,
        'language_id' => 2,
        'title' => 'API'
    ],
    [
        'section_id' => 101,
        'language_id' => 1,
        'title' => 'Категории'
    ],
    [
        'section_id' => 101,
        'language_id' => 2,
        'title' => 'Sections'
    ],
    [
        'section_id' => 1000,
        'language_id' => 1,
        'title' => 'Головна сторінка'
    ],
    [
        'section_id' => 1000,
        'language_id' => 2,
        'title' => 'Main Page'
    ],
    [
        'section_id' => 1100,
        'language_id' => 1,
        'title' => 'Новини'
    ],
    [
        'section_id' => 1100,
        'language_id' => 2,
        'title' => 'News'
    ],
    [
        'section_id' => 1101,
        'language_id' => 1,
        'title' => 'Фотографії'
    ],
    [
        'section_id' => 1101,
        'language_id' => 2,
        'title' => 'Images'
    ]
];