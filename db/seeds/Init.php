<?php

use Phinx\Seed\AbstractSeed;

class Init extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        // Set Languages
        $data = include(__DIR__ . '/init/languages.php');

        $language = $this->table('languages');
        $language
            ->insert($data)
            ->save();

        // Set Sections
        $data = require(__DIR__ . '/init/sections.php');
        $sections = $this->table('sections');
        $sections
            ->insert($data)
            ->save();

        // Set Sections i18n
        $data = include(__DIR__ .'/init/sections_i18n.php');
        $sections_i18n = $this->table('sections_i18n');
        $sections_i18n
            ->insert($data)
            ->save();

        // Set pre-filed articles
        $data = [
            [
                'id' => 1,
                'section_id' => 1100,
                'position' => 1,
                'is_active' => 1,
                'alias' => 'twig-for-developers'
            ],
            [
                'id' => 2,
                'section_id' => 1100,
                'position' => 2,
                'is_active' => 1,
                'alias' => 'db-seeding'
            ]
        ];
        $articles = $this->table('articles');
        $articles
            ->insert($data)
            ->save();

        // Set pre-filed i18n articles
        $data = [
            [
                'article_id' => 1,
                'language_id' => 1,
                'title' => 'Twig для розробників',
                'body' => 'Укр: This chapter describes the API to <i>Twig</i> and not the template language. It will be most useful as reference to those implementing the template interface to the application and not those who are creating Twig templates.'
            ],
            [
                'article_id' => 1,
                'language_id' => 2,
                'title' => 'Twig for Developers',
                'body' => 'Eng: This chapter describes the API to <i>Twig</i> and not the template language. It will be most useful as reference to those implementing the template interface to the application and not those who are creating Twig templates.'
            ],
            [
                'article_id' => 2,
                'language_id' => 1,
                'title' => 'Заповнення БД',
                'body' => 'Укр: In version 0.5.0 <b>Phinx</b> introduced support for seeding your database with test data. Seed classes are a great way to easily fill your database with data after its created. By default they are stored in the seeds directory, however this path can be changed in your configuration file.'
            ],
            [
                'article_id' => 2,
                'language_id' => 2,
                'title' => 'Database Seeding',
                'body' => 'Eng: In version 0.5.0 <b>Phinx</b> introduced support for seeding your database with test data. Seed classes are a great way to easily fill your database with data after its created. By default they are stored in the seeds directory, however this path can be changed in your configuration file.'
            ]
        ];
        $articles_i18n = $this->table('articles_i18n');
        $articles_i18n
            ->insert($data)
            ->save();

        // Set system's default roles
        $data = include(__DIR__ .'/init/user_roles.php');
        $roles= $this->table('roles');
        $roles
            ->insert($data)
            ->save();

        $_u   = array(1 => 'root', 2 => 'editor');
        $data = array();
        foreach($_u as $k => $value) {
            $password = \Meccano\Security\Hash::crypt('111');

            $data[] = [
                'id' => $k,
                'username' => $value,
                'is_active' => true,
                'pwd' => $password
            ];
        }
        $users = $this->table('users');
        $users
            ->insert($data)
            ->save();

        $data = [
            [
                'user_id' => 1,
                'role_id' => 1
            ],
            [
                'user_id' => 1,
                'role_id' => 4
            ],
            [
                'user_id' => 2,
                'role_id' => 2
            ],
            [
                'user_id' => 2,
                'role_id' => 3
            ]
        ];
        $users_roles = $this->table('users_roles');
        $users_roles
            ->insert($data)
            ->save();
    }
}
