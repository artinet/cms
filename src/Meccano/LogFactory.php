<?php

namespace Meccano;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogFactory
{
    public static function createLogService(Config $config, $logDir, $debug)
    {
        $logFilename = $logDir .'/system.log';
        $logLevel    = $debug ? Logger::DEBUG : Logger::ERROR;

        $fileStream = new StreamHandler($logFilename, $logLevel);

        if ($config->get('log.formatter')) {
            $formatter = new LineFormatter($config->get('log.formatter.output'), $config->get('log.formatter.date'));
            $fileStream->setFormatter($formatter);
        }

        $logger = new Logger('system');
        $logger->pushHandler($fileStream);
        $logger->pushHandler(new FirePHPHandler());

        return $logger;
    }
}
