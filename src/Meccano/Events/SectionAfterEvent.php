<?php

namespace Meccano\Events;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

class SectionAfterEvent extends Event
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, $section)
    {
        $this->container = $container;
    }

    public function getSection()
    {
        return $this->container->get('section');
    }
}
