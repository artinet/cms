<?php

namespace Meccano\Events;

use Meccano\Http\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

class ResponseEvent extends Event
{
    /** @var ContainerInterface */
    private $container;

    /** @var Response */
    private $response;

    public function __construct(ContainerInterface $container, Response $response)
    {
        $this->container = $container;
        $this->response  = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
}
