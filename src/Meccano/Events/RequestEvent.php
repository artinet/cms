<?php

namespace Meccano\Events;

use Meccano\Http\Request;
use Meccano\Http\Response;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;
use Symfony\Component\EventDispatcher\Event;

class RequestEvent extends Event
{
    /**
     * @var TaggedContainerInterface
     */
    private $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    public function __construct(TaggedContainerInterface $container, Request $request, Response $response)
    {
        $this->container = $container;
        $this->request   = $request;
        $this->response  = $response;
    }

    /**
     * Get Request
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return TaggedContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
}
