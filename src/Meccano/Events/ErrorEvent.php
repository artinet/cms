<?php

namespace Meccano\Events;


use Meccano\Http\Request;
use Meccano\Http\Response;
use Meccano\View\DataContainer;
use Symfony\Component\EventDispatcher\Event;

class ErrorEvent extends Event
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var DataContainer
     */
    private $data;

    /**
     * @var \Exception
     */
    private $exception;

    public function __construct(Request $request, Response $response, DataContainer $data, \Exception $exception)
    {
        $this->request   = $request;
        $this->response  = $response;
        $this->data      = $data;
        $this->exception = $exception;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return DataContainer
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }
}
