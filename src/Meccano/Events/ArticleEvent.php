<?php

namespace Meccano\Events;

use Meccano\Database\Model\Article;
use Symfony\Component\EventDispatcher\Event;

class ArticleEvent extends Event
{
    /**
     * @var Article
     */
    private $article;

    /**
     * ArticleEvent constructor.
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function getArticle()
    {
        return $this->article;
    }
}
