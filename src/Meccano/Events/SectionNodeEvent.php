<?php

namespace Meccano\Events;

use Meccano\Routing\Tree\Node;
use Symfony\Component\EventDispatcher\Event;

class SectionNodeEvent extends Event
{
    /**
     * @var Node
     */
    private $node;

    /**
     * SectionNodeEvent constructor.
     * @param Node $section
     */
    public function __construct(Node $section)
    {
        $this->node = $section;
    }

    /**
     * @return Node
     */
    public function getSection()
    {
        return $this->node;
    }
}
