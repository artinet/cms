<?php

namespace Meccano\Events;

use Meccano\Routing\Tree\Node;
use Symfony\Component\EventDispatcher\Event;

class SectionNodeGlobalEvent extends Event
{
    /**
     * @var Node
     */
    private $node;

    /**
     * @var Node
     */
    private $globalNode;

    /**
     * SectionNodeGlobalEvent constructor.
     * @param Node $node
     * @param Node $globalNode
     */
    public function __construct(Node $node, Node $globalNode)
    {
        $this->node       = $node;
        $this->globalNode = $globalNode;
    }

    /**
     * @return Node
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * @return Node
     */
    public function getGlobalNode()
    {
        return $this->globalNode;
    }
}
