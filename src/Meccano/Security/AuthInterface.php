<?php
namespace Meccano\Security;

use Meccano\Http\Request;
use Meccano\Security\Gateways\GatewayInterface;
use Meccano\Security\Storage\StorageInterface;
use Meccano\Security\Token\TokenInterface;
use Meccano\Security\Users\UserProviderInterface;
use Meccano\Security\Voters\VoteManagerInterface;

interface AuthInterface
{
    /**
     * Set the Request object
     * 
     * @param Request $request
     */
    public function setRequest(Request $request);
    
    /**
     * Set the Storage
     * 
     * @param StorageInterface $storage
     */
    public function setStorage(StorageInterface $storage);

    /**
     * Set UserProvider
     * @param UserProviderInterface $userProvider
     */
    public function setUserProvider(UserProviderInterface $userProvider);

    /**
     * Set Token
     * @param TokenInterface $token
     * @return mixed
     */
    public function setToken(TokenInterface $token);
    
    /**
     * Perform the authorization actions
     * @param VoteManagerInterface $voteManager
     * @return TokenInterface
     */
    public function authorize(VoteManagerInterface $voteManager);

    /**
     * Perform the authentication actions
     * @param GatewayInterface $gateway
     * @return TokenInterface
     */
    public function authenticate(GatewayInterface $gateway);
}
