<?php
namespace Meccano\Security\Subscribers;


use Meccano\Config;
use Meccano\Security\AuthFactory;
use Meccano\Security\Users\UserProviderInterface;
use Meccano\View\DataContainer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Meccano\MeccanoEvents;
use Meccano\Events\RequestEvent;
use Meccano\Security\Storage\SessionAdapter;
use Meccano\Security\Storage\StorageInterface;
use Meccano\Security\Gateways\GatewayFactory;
use Meccano\Security\Events\SecurityEvents;
use Meccano\Security\Events\LoginEvent;
use Meccano\Security\Events\LogedEvent;

class SecurityPathSubscriber implements EventSubscriberInterface
{
    private static $loginPath = '';
    private static $logoutPath = '';
    
    public static function setLoginPath($path)
    {
        self::$loginPath = $path;
    }
    
    public static function setLogoutPath($path)
    {
        self::$logoutPath = $path;
    }

    public static function getSubscribedEvents()
    {
        return [
            MeccanoEvents::getUrlEventName(self::$loginPath) => 'onLoginRequest',
            MeccanoEvents::getUrlEventName(self::$logoutPath) => 'onLogoutRequest',
        ];
    }
    
    public function onLoginRequest(RequestEvent $event)
    {
        $request  = $event->getRequest();
        $response = $event->getResponse();

        if ($request->isMethod('POST')) {
            $storage = new SessionAdapter($request->getSession());
            $eventDispatcher = $event->getContainer()->get('events');
            
            $eventDispatcher->dispatch(
                SecurityEvents::LOGIN_BEFORE,
                new LoginEvent($event->getContainer()->get('data'), $storage)
            );

            /** @var Config $config */
            $config = $event->getContainer()->get('config');
            $loginGateway = GatewayFactory::create(
                $request,
                $config
            );

            /** @var UserProviderInterface $userProvider */
            $userProvider = $event->getContainer()->get('security.user.provider');

            $token = AuthFactory::create(
                $request,
                $response,
                $userProvider,
                $event->getContainer()->get('config')->get('security')
            )
                ->authenticate($loginGateway);

            /** @var DataContainer $dataContainer */
            $dataContainer = $event->getContainer()->get('data');
            $dataContainer->setContent('user', $token->getUser());

            $eventDispatcher->dispatch(
                SecurityEvents::LOGIN_AFTER,
                new LogedEvent($event->getContainer()->get('data'), $token, $storage)
            );
        }
    }
    
    public function onLogoutRequest(RequestEvent $event)
    {
        $storage = new SessionAdapter($event->getRequest()->getSession());
        
        /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $eventDispatcher = $event->getContainer()->get('events');
        
        $eventDispatcher->dispatch(
            SecurityEvents::LOGOUT_BEFORE,
            new LoginEvent($event->getContainer()->get('data'), $storage)
        );
        
        $storage->remove(StorageInterface::STORAGE_USER_TOKEN);
        
        $eventDispatcher->dispatch(
            SecurityEvents::LOGOUT_AFTER,
            new LoginEvent($event->getContainer()->get('data'), $storage)
        );

        return true;
    }
}
