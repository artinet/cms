<?php
namespace Meccano\Security\Subscribers;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Meccano\Security\Events\SecurityEvents;
use Meccano\Security\Events\LogedEvent;

class LoggedInSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     * @see EventSubscriberInterface::getSubscribedEvents()
     */
    public static function getSubscribedEvents()
    {
        return [
            SecurityEvents::LOGIN_AFTER => 'onLoggedIn'
        ];
    }

    public function onLoggedIn(LogedEvent $event)
    {
        echo 'in loged IN'; exit();
        $token = $event->getToken();
        $data  = $event->getDataContainer();
        
        var_dump($token->getUser());exit();
        $data->setContent('user', $token->getUser());
    }
}
