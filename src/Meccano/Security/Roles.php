<?php
namespace Meccano\Security;


class Roles
{
    const ROLE_ROOT = 'ROOT';
    const ROLE_ADMIN = 'ADMIN';
    const ROLE_USER = 'USER';
    const ROLE_SUBSCRIBER = 'SUBSCRIBER';
}
