<?php
namespace Meccano\Security\Token;


use Meccano\Security\Users\UserInterface;

class SessionToken implements TokenInterface
{
    /**
     * @var UserInterface
     */
    private $user;
    
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $signature = null;
    
    public function __sleep()
    {
        return ['username', 'signature'];
    }
    
    /**
     * {@inheritDoc}
     * @see TokenInterface::setUser()
     */
    public function setUser(UserInterface $user)
    {
        $signature = $this->generateUserSignature($user);

        if (!$this->signature) {
            $this->signature = $signature;
        }

        if ($this->signature === $signature) {
            $this->user = $user;
            $this->username = $user->getUsername();
        } else {
            $this->username = null;
            $this->signature = null;
        }
    }

    /**
     * {@inheritDoc}
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     * @see TokenInterface::clearUser()
     */
    public function clearUser()
    {
        $this->user     = null;
        $this->username = null;
    }

    /**
     * {@inheritDoc}
     * @see TokenInterface::getUsername()
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Generate the User's signature
     *
     * @param UserInterface $user
     * @return string
     */
    private function generateUserSignature(UserInterface $user)
    {
        if (!$user->getUsername()) {
            return null;
        }

        $key_str = $user->getUsername() . $user->getPassword();

        return sha1($key_str);
    }
}
