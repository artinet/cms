<?php

namespace Meccano\Security\Token;


use Meccano\Security\Users\UserInterface;

class JwtToken implements TokenInterface
{
    const ENC_METHOD = 'AES256';
    const KEY_USERNAME = 'username';
    const KEY_EXPIRATION = 'expiration';

    /**
     * @var string
     */
    private $key;

    /**
     * @var integer
     */
    private $expiration;

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $income_token;

    /**
     * @var UserInterface
     */
    private $user;

    public function __construct($key, $expiration, $token)
    {
        $this->key          = $key;
        $this->expiration   = (integer)$expiration;
        $this->income_token = $token;

        $this->data = $this->hydrateToken();
    }

    public function __toString()
    {
        return $this->dehydrateToken();
    }

    /**
     * {@inheritdoc}
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     * @see TokenInterface::clearUser()
     */
    public function clearUser()
    {
        $this->data = [];
        $this->user = null;
    }

    /**
     * {@inheritdoc}
     * @see TokenInterface::getUsername()
     */
    public function getUsername()
    {
        if (!empty($this->data) && array_key_exists(self::KEY_USERNAME, $this->data)) {
            return $this->data[self::KEY_USERNAME];
        }

        if (!empty($this->user)) {
            return $this->user->getUsername();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     * @see TokenInterface::setUser()
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        $this->data = [
            self::KEY_USERNAME => $user->getUsername(),
            self::KEY_EXPIRATION => time() + $this->expiration
            // TODO: add something new in the token (some additional user's value)
        ];
    }

    private function dehydrateToken()
    {
        $token_str = json_encode($this->data);
        return openssl_encrypt($token_str, self::ENC_METHOD, $this->key);
    }

    private function hydrateToken()
    {
        $token_str  = openssl_decrypt($this->income_token, self::ENC_METHOD, $this->key);
        $data = json_decode($token_str, true);

        if (empty($data[self::KEY_EXPIRATION]) || $data[self::KEY_EXPIRATION] < time())
        {
            $data = [];
        }

        return $data;
    }
}
