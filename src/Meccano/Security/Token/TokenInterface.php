<?php
namespace Meccano\Security\Token;

use Meccano\Security\Users\UserInterface;

interface TokenInterface
{
    const USER_ANONYMOUS = 0;
    const USER_AUTHORIZED = 1;
    
    /**
     * Set User
     * @param UserInterface $user
     */
    public function setUser(UserInterface $user);
    
    /**
     * Get User
     * @return UserInterface
     */
    public function getUser();

    /**
     * Clear the User in a token
     */
    public function clearUser();
    
    /**
     * Get username
     * @return string
     */
    public function getUsername();
}
