<?php

namespace Meccano\Security;


use Meccano\Http\Request;
use Meccano\Http\Response;
use Meccano\Security\Storage\RequestAdapter;
use Meccano\Security\Storage\SessionAdapter;
use Meccano\Security\Storage\StorageInterface;
use Meccano\Security\Token\JwtToken;
use Meccano\Security\Token\SessionToken;
use Meccano\Security\Token\TokenInterface;
use Meccano\Security\Users\UserProviderInterface;

class AuthFactory
{
    const AUTH_MODE_SESSION = 'session';
    const AUTH_MODE_JWT = 'jwt';

    /**
     * Authentication method
     * @var string
     */
    private static $mode = 'session';

    public static function create(Request $request, Response $response, UserProviderInterface $provider, array $config)
    {
        self::detectMode($request, $config);

        $storage = self::createStorage($request, $response, $config);
        $token   = self::createToken($storage, $config);

        $auth = new Auth();
        $auth->setRequest($request)
            ->setUserProvider($provider)
            ->setStorage($storage)
            ->setToken($token);

        return $auth;
    }

    private function detectMode(Request $request, $config)
    {
        if (!empty($config['jwt']['name']) && !empty($config['jwt']['enable_jwt_header'])) {
            if ($request->headers->get($config['jwt']['enable_jwt_header'])) {
                self::$mode = self::AUTH_MODE_JWT;
                return;
            }
        }

        self::$mode = self::AUTH_MODE_SESSION;
        return;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $config
     * @return StorageInterface
     */
    private static function createStorage(Request $request, Response $response, $config)
    {
        if (self::$mode == self::AUTH_MODE_JWT)
        {
            return new RequestAdapter($request, $response, $config['jwt']['name']);
        } else
        {
            return new SessionAdapter($request->getSession());
        }
    }

    /**
     * Create TokenInterface
     * @param StorageInterface $storage
     * @return TokenInterface
     */
    private static function createToken(StorageInterface $storage, $config)
    {
        /** @var TokenInterface $token */
        $token = $storage->get($storage->getSaveName());

        if (!$token || !($token instanceof TokenInterface)) {
            if (self::$mode == self::AUTH_MODE_JWT) {
                $token = new JwtToken($config['key'], $config['jwt']['expiration'], $token);
            } else {
                $token = new SessionToken();
            }
            $storage->set($storage->getSaveName(), $token);
        }

        return $token;
    }
}