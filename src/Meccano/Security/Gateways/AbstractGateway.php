<?php
namespace Meccano\Security\Gateways;


use Meccano\Http\Request;
use Meccano\Config;

abstract class AbstractGateway implements GatewayInterface
{
    /**
     * @var Config
     */
    protected $config;
    
    /**
     * @var Request
     */
    protected $request;
    
    /**
     * @var string
     */
    protected $configy_key = 'security';
    
    /**
     * {@inheritDoc}
     * @see GatewayInterface::setConfig()
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see GatewayInterface::setRequest()
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        
        return $this;
    }
    
    abstract function getUsername();
    abstract function getPassword();
}
