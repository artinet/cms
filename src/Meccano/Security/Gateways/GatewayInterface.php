<?php
namespace Meccano\Security\Gateways;


use Meccano\Http\Request;
use Meccano\Config;

interface GatewayInterface
{
    /**
     * @param Request $request
     */
    public function setRequest(Request $request);
    
    /**
     * @param Config $config
     */
    public function setConfig(Config $config);
    
    /**
     * @return string
     */
    public function getUsername();
    
    /**
     * @return string
     */
    public function getPassword();
}
