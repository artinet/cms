<?php
namespace Meccano\Security\Gateways;


class FormGateway extends AbstractGateway
{
    /**
     * {@inheritDoc}
     * @see GatewayInterface::getUsername()
     */
    public function getUsername()
    {
        $u_key = $this->config->get($this->configy_key .'.login.username');
        
        return $this->request->request->get($u_key);
    }
    
    /**
     * {@inheritDoc}
     * @see GatewayInterface::getPassword()
     */
    public function getPassword()
    {
        $p_key = $this->config->get($this->configy_key .'.login.password');
        
        return $this->request->request->get($p_key);
    }
}
