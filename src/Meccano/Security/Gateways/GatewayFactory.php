<?php
namespace Meccano\Security\Gateways;


use Meccano\Http\Request;
use Meccano\Config;

class GatewayFactory
{
    /**
     * @param Request $request
     * @return GatewayInterface
     */
    public static function create(Request $request, Config $config)
    {
        $contentType = $request->headers->get('content-type', 'application/x-www-form-urlencoded');
       
        if($contentType == 'application/json') {
            $gateway = new ApiGateway();
        } else 
        {
            $gateway = new FormGateway();
        }
        
        $gateway->setConfig($config);
        $gateway->setRequest($request);
        
        return $gateway;
    }
}
