<?php
namespace Meccano\Security\Voters;


use Meccano\Http\Request;
use Meccano\Security\Token\TokenInterface;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;

interface VoteManagerInterface
{
    /**
     * @param TaggedContainerInterface $container
     */
    public function __construct(TaggedContainerInterface $container);
    
    /**
     * Detect if the user is authorized
     * 
     * @param Request $request
     * @param TokenInterface $token
     */
    public function process(Request $request, TokenInterface $token);
}
