<?php
namespace Meccano\Security\Voters;


use Meccano\Config;
use Meccano\Http\Request;
use Meccano\Security\Token\TokenInterface;

class ConfigSecurityVoter implements VoterInterface
{
    /**
     * @var array
     */
    private $securityRules = [];
    
    /**
     * @param Config $config
     */
    public function setRules(Config $config)
    {
        $this->securityRules = $config->get('security.rules');
    }

    /**
     * {@inheritDoc}
     * @see VoterInterface::vote()
     */
    public function vote(Request $request, TokenInterface $userToken)
    {
        foreach ($this->securityRules as $rule) {
            $expr = '/' . addcslashes($rule['path'], '/') . '/';

            if (preg_match($expr, $request->getPathInfo())) {
                $common_roles = array_intersect($rule['roles'], $userToken->getUser()->getRoles());
                if (sizeof($common_roles) > 0) {
                    return self::ACCESS_GRANTED;
                } else {
                    return self::ACCESS_REJECTED;
                }
            }
        }

        return self::ACCESS_UNDEFINED;
    }
}
