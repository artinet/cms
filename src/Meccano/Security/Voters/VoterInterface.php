<?php

namespace Meccano\Security\Voters;

use Meccano\Http\Request;
use Meccano\Security\Token\TokenInterface;

interface VoterInterface
{
    const ACCESS_GRANTED   = true;
    const ACCESS_REJECTED  = false;
    const ACCESS_UNDEFINED = null;
    
    /**
     * Vote if the user has access
     * 
     * @param Request $request
     * @param TokenInterface $token
     * @return bool
     */
    public function vote(Request $request, TokenInterface $token);
}
