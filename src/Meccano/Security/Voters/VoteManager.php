<?php
namespace Meccano\Security\Voters;


use Meccano\Http\Request;
use Meccano\Security\Token\TokenInterface;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;
use Meccano\Security\Exceptions\AccessDenied;

class VoteManager implements VoteManagerInterface
{
    /**
     * @var TaggedContainerInterface
     */
    private $container;
    
    /**
     * @var array
     */
    private $voters = [];
    
    /**
     * @param TaggedContainerInterface $container
     */
    public function __construct(TaggedContainerInterface $container)
    {
        $this->container = $container;

        $voters = $container->findTaggedServiceIds('security.voter');
        
        foreach ($voters as $id => $tags) {
            $this->voters[] = $id;
        }
    }
    
    /**
     * {@inheritDoc}
     * @see VoteManagerInterface::process()
     */
    public function process(Request $request, TokenInterface $token)
    {
        foreach ($this->voters as $id) {
            $voteService = $this->container->get($id);
            $voteResults = $voteService->vote($request, $token);

            if ($voteResults === VoterInterface::ACCESS_GRANTED) {
                return true;
            } elseif ($voteResults === VoterInterface::ACCESS_REJECTED) {
                throw new AccessDenied();
            }
        }
        
        return true;
    }
}
