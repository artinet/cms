<?php

namespace Meccano\Security;

use Meccano\Http\Request;
use Meccano\Security\Exceptions\UserCredentialExceptions;
use Meccano\Security\Gateways\GatewayInterface;
use Meccano\Security\Storage\StorageInterface;
use Meccano\Security\Token\TokenInterface;
use Meccano\Security\Users\UserInterface;
use Meccano\Security\Users\UserProviderInterface;
use Meccano\Security\Users\AnonymousUser;
use Meccano\Security\Voters\VoteManagerInterface;
use Meccano\Security\Exceptions\AccessDenied;

class Auth implements AuthInterface
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * @var TokenInterface
     */
    private $token;

    /**
     * {@inheritdoc}
     * @see AuthInterface::setRequest()
     */
    public function setRequest(Request $request) 
    {
        $this->request = $request;
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see AuthInterface::setStorage()
     */
    public function setStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see AuthInterface::setUserProvider()
     */
    public function setUserProvider(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
        
        return $this;
    }

    /**
     * {@inheritdoc}
     * @see AuthInterface::setToken()
     */
    public function setToken(TokenInterface $token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * {@inheritdoc}
     * @see AuthInterface::authorize()
     */
    public function authorize(VoteManagerInterface $voteManager)
    {
        $token = $this->token;

        if ($token->getUsername()) {
            // Validate UserToken
            /** @var UserInterface $user */
            $user = $this->userProvider->getUser($token->getUsername());

            if (!$user || !$user->isActive()) {
                throw new AccessDenied();
            }
        } else {
            // Create anonymous user
            $user = new AnonymousUser();
        }

        $token->setUser($user);

        // Check permissions
        $voteManager->process($this->request, $token);
        
        return $token;
    }

    /**
     * {@inheritdoc}
     * @see AuthInterface::authenticate()
     */
    public function authenticate(GatewayInterface $gateway)
    {
        if (!$gateway->getUsername() || !$gateway->getPassword()) {
            throw new UserCredentialExceptions();
        }

        /** @var UserInterface $user */
        $user = $this->userProvider->getUser($gateway->getUsername());

        if (!$user || !Hash::compare($gateway->getPassword(), $user->getPassword())) {
            throw new AccessDenied();
        }

        $this->token->setUser($user);
        
        return $this->token;
    }
}
