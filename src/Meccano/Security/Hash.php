<?php

namespace Meccano\Security;

class Hash
{
    private static $system_key;

    /**
     * Set system's kay
     * @param string $key
     */
    public static function setSystemKey($key)
    {
        self::$system_key = $key;
    }

    /**
     * Generate random string
     * @param int $length
     * @return string
     */
    public static function generateSalt($length)
    {
        if (!isset($length) || intval($length) <= 8) {
            $length = 32;
        }

        if (function_exists('random_bytes')) {
            return bin2hex(random_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
    }

    /**
     * Make hash of the string using salt and system's salt
     *
     * @param string $string
     * @return string
     */
    public static function crypt($string)
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }

    /**
     * Compare string to its hash
     *
     * @param string $string
     * @param string $hash
     * @return bool
     */
    public static function compare($string, $hash)
    {
        return password_verify($string, $hash);
    }
}
