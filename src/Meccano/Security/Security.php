<?php

namespace Meccano\Security;

use Meccano\Http\Request;
use Meccano\Http\Response;
use Meccano\Security\Users\UserProviderInterface;
use Meccano\Security\Voters\VoteManager;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;
use Meccano\View\DataContainer;
use Meccano\Security\Subscribers\SecurityPathSubscriber;
use Meccano\Security\Events\SecurityEvents;
use Meccano\Security\Events\LogedEvent;

class Security
{
    const SECURITY_VOTER_TAG = 'security.voter';
    const SECURITY_USER_PROVIDER_SERVICE = 'security.user.provider';

    /**
     * @var TaggedContainerInterface
     */
    private $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;
    
    /**
     * @var DataContainer
     */
    private $dataContainer;

    /**
     * @param TaggedContainerInterface $container
     * @return $this
     */
    public function setContainer(TaggedContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param Response $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
        return $this;
    }

    public function execute()
    {
        $this->registerSecurityServices();
        $this->registerSecurityEvents();

        /** @var UserProviderInterface $userProvider */
        $userProvider  = $this->container->get(self::SECURITY_USER_PROVIDER_SERVICE);

        AuthFactory::create(
                $this->request,
                $this->response,
                $userProvider,
                $this->container->get('config')->get('security')
            )
            ->authorize(
                new VoteManager($this->container)
            );
    }

    private function registerSecurityServices()
    {
        $this->container->register(self::SECURITY_VOTER_TAG . '.config', 'Meccano\Security\Voters\ConfigSecurityVoter')
            ->addMethodCall('setRules', array(new Reference('config')))
            ->addTag(self::SECURITY_VOTER_TAG);

        if (!$this->container->has(self::SECURITY_USER_PROVIDER_SERVICE)) {
            $this->container->register(self::SECURITY_USER_PROVIDER_SERVICE, 'Meccano\Security\Users\UserProvider')
                ->addMethodCall('setDatabase', [new Reference('database')]);
        }
    }

    private function registerSecurityEvents()
    {
        $configuration = $this->container->get('config')->get('security');

        SecurityPathSubscriber::setLoginPath($configuration['login']['url']);
        SecurityPathSubscriber::setLogoutPath($configuration['logout']['url']);
        $eventDispatcher = $this->container->get('events');
        
        $eventDispatcher->addSubscriber(new SecurityPathSubscriber());
    }
}
