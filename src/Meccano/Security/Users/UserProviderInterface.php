<?php
namespace Meccano\Security\Users;


interface UserProviderInterface
{

    /**
     * Get User by Username
     * @param string $username
     * @return UserInterface
     */
    public function getUser($username);
}
