<?php
namespace Meccano\Security\Users;


use Meccano\Database\Db;

class UserProvider implements UserProviderInterface
{
    /**
     * @var Db
     */
    private $db;
    
    public function setDatabase(Db $db)
    {
        $this->db = $db;
    }

    /**
     * {@inheritDoc}
     * @see UserProviderInterface::getUser()
     */
    public function getUser($username)
    {
        return $this->db->model('User')
            ->where('username', $username)
            ->getOne();
    }
}
