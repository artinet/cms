<?php

namespace Meccano\Security\Users;

interface UserInterface
{   
    /**
     * Get User's ID
     * @return string
     */
    public function getId();
    
    /**
     * Get Username
     * @return string
     */
    public function getUsername();
    
    /**
     * Get User's password hash
     * @return string
     */
    public function getPassword();
    
    /**
     * Get User's state
     * @return bool
     */
    public function isActive();
    
    /**
     * Get User's roles
     * @return array
     */
    public function getRoles();
}
