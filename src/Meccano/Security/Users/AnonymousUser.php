<?php
namespace Meccano\Security\Users;


class AnonymousUser implements UserInterface
{
    /**
     * {@inheritDoc}
     * @see UserInterface::getId()
     */
    public function getId()
    {
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @see UserInterface::getUsername()
     */
    public function getUsername()
    {
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @see UserInterface::getPassword()
     */
    public function getPassword()
    {
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @see UserInterface::isActive()
     */
    public function isActive()
    {
        return true;
    }
    
    /**
     * {@inheritDoc}
     * @see UserInterface::getRoles()
     */
    public function getRoles()
    {
        return [];
    }
}
