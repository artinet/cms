<?php
namespace Meccano\Security\Events;

class SecurityEvents
{
    const LOGIN_BEFORE  = 'login.before';
    const LOGIN_AFTER   = 'login.after';
    const LOGOUT_BEFORE = 'logout.before';
    const LOGOUT_AFTER  = 'logout.after';
    const AUTH_FAILED   = 'auth.failed';
}
