<?php
namespace Meccano\Security\Events;


use Symfony\Component\EventDispatcher\Event;
use Meccano\View\DataContainer;
use Meccano\Security\Token\TokenInterface;
use Meccano\Security\Storage\StorageInterface;

class LogedEvent extends Event
{
    /**
     * @var DataContainer
     */
    private $data;
    
    /**
     * @var TokenInterface
     */
    private $token;
    
    /**
     * @var StorageInterface
     */
    private $storage;
    
    public function __construct(DataContainer $data, TokenInterface $token) 
    {
        $this->data    = $data;
        $this->token   = $token;
        $this->storage = $storage;
    }
    
    /**
     * @return DataContainer
     */
    public function getDataContainer()
    {
        return $this->data;
    }
    
    /**
     * @return TokenInterface
     */
    public function getToken()
    {
        return $this->token;
    }
    
    /**
     * @return StorageInterface
     */
    public function getStorage()
    {
        return $this->storage;
    }
}
