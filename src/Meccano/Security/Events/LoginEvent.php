<?php
namespace Meccano\Security\Events;


use Symfony\Component\EventDispatcher\Event;
use Meccano\Security\Storage\StorageInterface;
use Meccano\View\DataContainer;

class LoginEvent extends Event
{
    /**
     * @var DataContainer
     */
    private $data;
    
    /**
     * @var StorageInterface
     */
    private $storage;
    
    public function __construct(DataContainer $dataContainer, StorageInterface $storage)
    {
        $this->data    = $dataContainer;
        $this->storage = $storage;
    }
    
    /**
     * @return DataContainer
     */
    public function getDataContainer()
    {
        return $this->data;
    }
    
    /**
     * @return StorageInterface
     */
    public function getStorage()
    {
        return $this->storage;
    }
}
