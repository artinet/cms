<?php
namespace Meccano\Security\Exceptions;


use Meccano\Exceptions\SystemConfigurationException;

class UserProviderException extends SystemConfigurationException
{
    public function __construct($message=null, $code=null, $previous=null)
    {
        if (!$message) {
            $message = "User Provider should be defined and implement the interface of [Meccano\Security\Users\UserProviderInterface]";
        }
        
        parent::__construct($message, $code, $previous);
    }
}
