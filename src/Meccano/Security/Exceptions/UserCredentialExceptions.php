<?php

namespace Meccano\Security\Exceptions;


use Meccano\Exceptions\Error406Exception;

class UserCredentialExceptions extends Error406Exception
{
    public function __construct($message=null, $code=null, $previous=null)
    {
        if (!$message) {
            $message = "User haven't provided credentials or provided credentials are wrong";
        }

        parent::__construct($message, $code, $previous);
    }
}
