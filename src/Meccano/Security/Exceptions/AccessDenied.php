<?php
namespace Meccano\Security\Exceptions;


use Meccano\Exceptions\Error403Exception;

class AccessDenied extends Error403Exception
{
    public function __construct($message=null, $code=null, $previous=null)
    {
        if (!$message) {
            $message = "Access denied";
        }
        
        parent::__construct($message, $code, $previous);
    }
}
