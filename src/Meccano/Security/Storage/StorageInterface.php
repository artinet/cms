<?php
namespace Meccano\Security\Storage;


interface StorageInterface
{
    const STORAGE_USER_TOKEN = 'security.user';

    /**
     * Save the value in a Storage
     * 
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value);
    
    /**
     * Retrive the value from a Storage
     * @param string $name
     */
    public function get($name);
    
    /**
     * Remove the value in a Storage
     * @param string $name
     */
    public function remove($name);

    /**
     * Get the saving name in the storage
     * @return string
     */
    public function getSaveName();
}
