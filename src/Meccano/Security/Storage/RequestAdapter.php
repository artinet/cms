<?php

namespace Meccano\Security\Storage;


use Meccano\Http\Request;
use Meccano\Http\Response;

class RequestAdapter implements StorageInterface
{
    /** @var Request */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /** @var string */
    private $header_name;

    /**
     * RequestAdapter constructor.
     * @param Request $request
     * @param Response $response
     * @param $header_name
     */
    public function __construct(Request $request, Response $response, $header_name)
    {
        $this->request     = $request;
        $this->response    = $response;
        $this->header_name = $header_name;
    }

    /**
     * {@inheritdoc}
     * @see StorageInterface::set()
     */
    public function set($name, $value)
    {
        $this->response->headers->set($name, $value);
    }

    /**
     * {@inheritdoc}
     * @see StorageInterface::get()
     */
    public function get($name)
    {
        return $this->request->headers->get($name);
    }

    /**
     * {@inheritdoc}
     * @see StorageInterface::remove()
     */
    public function remove($name)
    {
        $this->request->headers->remove($name);
        $this->response->headers->remove($name);
    }

    /**
     * {@inheritdoc}
     * @see StorageInterface::getSaveName()
     */
    public function getSaveName()
    {
        return $this->header_name;
    }
}