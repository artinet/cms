<?php
namespace Meccano\Security\Storage;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionAdapter implements StorageInterface
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    /**
     * {@inheritDoc}
     * @see StorageInterface::set()
     */
    public function set($name, $value) 
    {
        return $this->session->set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @see StorageInterface::get()
     */
    public function get($name) 
    {
        return $this->session->get($name);
    }

    /**
     * {@inheritDoc}
     * @see StorageInterface::remove()
     */
    public function remove($name) 
    {
        return $this->session->remove($name);
    }

    /**
     * {@inheritdoc}
     * @see StorageInterface::getSaveName()
     */
    public function getSaveName()
    {
        return self::STORAGE_USER_TOKEN;
    }
}
