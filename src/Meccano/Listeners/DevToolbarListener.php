<?php

namespace Meccano\Listeners;

use Meccano\Events\ResponseEvent;
use Meccano\MeccanoEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Meccano\Http\Request;

class DevToolbarListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            MeccanoEvents::RESPONSE => array('onRespond', -255)
        );
    }

    public function onRespond(ResponseEvent $event)
    {
        $response  = $event->getResponse();
        $container = $event->getContainer();

        $request = $container->get('request');
        
        $content = $response->getContent();
        if ($request->getAcceptableType() === Request::CONTENT_EXT_HTML) {
            $content .= '<script>alert("it is a dev toolbar")</script>';
            $response->setContent($content);
        }
    }
}
