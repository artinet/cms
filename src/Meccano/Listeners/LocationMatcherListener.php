<?php

namespace Meccano\Listeners;

use Meccano\Events\RequestEvent;
use Meccano\MeccanoEvents;
use Meccano\Routing\LocationMatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocationMatcherListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            MeccanoEvents::REQUEST => ['onRequest', 255]
        ];
    }

    /**
     * OnRequest event listener method
     *
     * @param RequestEvent $event
     * @return \Meccano\Http\Request
     */
    public function onRequest(RequestEvent $event)
    {
        $locationMatcher = new LocationMatcher();
        $locationMatcher->setContainer($event->getContainer());

        return $locationMatcher->match($event->getRequest());
    }
}
