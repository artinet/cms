<?php

namespace Meccano\Listeners;


use Meccano\Events\ErrorEvent;
use Meccano\Exceptions;
use Meccano\Http\Request;
use Meccano\Http\Response;
use Meccano\MeccanoEvents;
use Meccano\View\DataContainer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ErrorListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            MeccanoEvents::RESPONSE_ERROR => ['onError']
        ];
    }

    public function onError(ErrorEvent $event)
    {
        /** @var \Exception $exception */
        $exception = $event->getException();
        $request   = $event->getRequest();
        $response  = $event->getResponse();
        $data      = $event->getData();

        if ($exception instanceof Exceptions\RedirectException) {
            $this->redirectResponse($request, $response);
        } else {
            $this->errorResponse($request, $response, $data, $exception);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    private function redirectResponse(Request $request, Response $response)
    {
        $response->prepare($request);

        return $response;
    }



    /**
     * @param Request $request
     * @param Response $response
     * @param DataContainer $data
     * @param \Exception $exception
     * @return Response
     */
    private function errorResponse(Request $request, Response $response, DataContainer $data, \Exception $exception)
    {
        $response->prepare($request);

        if ($exception instanceof Exceptions\Error404Exception) {
            $data['template'] = 'errors/error404.html';
        } elseif ($exception instanceof Exceptions\Error401Exception) {
            $data['template'] = 'errors/error401.html';
        } else {
            $data['template'] = 'errors/error500.html';
        }
        
        if ($exception->getCode() > 99 && $exception->getCode() < 600) {
            $response->setStatusCode($exception->getCode());
        } else {
            $response->setStatusCode(500);
        }

        $data->setContent('exception', $exception);
    }
}
