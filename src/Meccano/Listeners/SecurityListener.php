<?php
namespace Meccano\Listeners;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Meccano\Events\RequestEvent;
use Meccano\MeccanoEvents;
use Meccano\Security\Security;

class SecurityListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            MeccanoEvents::REQUEST => array('onRequest', 250)
        );
    }
    
    public function onRequest(RequestEvent $event)
    {
        $security = new Security();
        $security->setContainer($event->getContainer())
            ->setRequest($event->getRequest())
            ->setResponse($event->getResponse())
            ->execute();
    }
}
