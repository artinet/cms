<?php
namespace Meccano\Exceptions;

class Error415Exception extends MeccanoException
{
    public function __construct($message='Unsupported Media Type', $code=null, $previous=null)
    {        
        parent::__construct($message, 415, $previous);
    }
}
