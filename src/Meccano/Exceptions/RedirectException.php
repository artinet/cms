<?php
namespace Meccano\Exceptions;

class RedirectException extends MeccanoException
{
    private $redirectStatusCode;
    
    public function setRedirectStatusCode($statusCode)
    {
        $this->redirectStatusCode = $statusCode;
        
        return $this;
    }
    
    public function getRedirectStatusCode() 
    {
        return $this->redirectStatusCode;
    }
}
