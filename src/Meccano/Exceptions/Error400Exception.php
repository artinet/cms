<?php
namespace Meccano\Exceptions;


class Error400Exception extends MeccanoException
{
    public function __construct($message='Bad Request', $code=null, $previous=null)
    {
        parent::__construct($message, 400, $previous);
    }
}
