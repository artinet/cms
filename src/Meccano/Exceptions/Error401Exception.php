<?php

namespace Meccano\Exceptions;

class Error401Exception extends MeccanoException
{
    public function __construct($message='Unauthorized', $code=null, $previous=null)
    {
        parent::__construct($message, 401, $previous);
    }
}
