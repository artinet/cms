<?php

namespace Meccano\Exceptions;

class Error403Exception extends MeccanoException
{
    public function __construct($message='Forbidden', $code=null, $previous=null)
    {
        parent::__construct($message, 403, $previous);
    }
}
