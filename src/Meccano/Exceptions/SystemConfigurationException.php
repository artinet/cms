<?php
namespace Meccano\Exceptions;

class SystemConfigurationException extends Error500Exception
{
    public function __construct($message = null, $code = null, $previous = null)
    {
        if(!$message) {
            $message = 'System Configuration Falty';
        }
        
        parent::__construct($message, $code, $previous);
    }
}
