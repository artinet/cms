<?php

namespace Meccano\Exceptions;

class Error406Exception extends MeccanoException
{
    public function __construct($message='Not Acceptable', $code=null, $previous=null)
    {
        parent::__construct($message, 406, $previous);
    }
}
