<?php

namespace Meccano\Exceptions;

class Error500Exception extends MeccanoException
{
    public function __construct($message='Internal Server Error', $code=500, $previous=null) 
    {
        parent::__construct($message, $code, $previous);
    }
}
