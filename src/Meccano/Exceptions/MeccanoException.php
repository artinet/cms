<?php
namespace Meccano\Exceptions;

class MeccanoException extends \Exception implements \JsonSerializable
{
    /**
     * {@inheritdoc}
     *
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        return [
            'code'    => $this->getCode(),
            'message' => $this->getMessage()
        ];
    }
}
