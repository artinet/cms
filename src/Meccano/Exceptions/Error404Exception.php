<?php

namespace Meccano\Exceptions;

class Error404Exception extends MeccanoException
{
    public function __construct($message='Not Found', $code=null, $previous=null)
    {
        parent::__construct($message, 404, $previous);
    }
}
