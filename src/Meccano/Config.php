<?php

namespace Meccano;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Config
 */
class Config
{

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var array
     */
    private $configValues;

    /**
     * Config constructor.
     * Build configs from Files
     *
     * @param string             $location
     * @param string             $environment
     * @param ContainerInterface $container
     */
    public function __construct($location, $environment, ContainerInterface $container = null)
    {
        $this->location    = (array) $location;
        $this->environment = $environment;

        $this->loadYamlConfig();

        if (empty($this->configValues['app'])) {
            $this->configValues['app'] = array();
        }

        if (!empty($container)) {
            $this->configValues['app'] = array_merge_recursive(
                $this->configValues['app'],
                array(
                    'path' => array(
                        'root' => $container->getParameter('path.root'),
                        'webroot' => $container->getParameter('path.webroot')
                    ),
                    'debug' => $container->getParameter('debug'),
                    'environment' => $container->getParameter('app.environment')
                )
            );

            $this->configValues['security']['key'] = file_get_contents(
                $container->getParameter('path.root') .'/.key'
            );
            
            // Override system config with app
            foreach ($this->configValues as $key => $value) {
                if (isset($this->configValues['app'][$key])) {
                    $this->configValues[$key] = array_replace_recursive(
                        $value, 
                        $this->configValues['app'][$key]
                    );
                }
            }

            // TODO: load configs from DB
        }
    }

    /**
     * Get config value
     *
     * @param $name string
     * @return array|scalar
     */
    public function get($name)
    {
        $name = explode('.', $name);

        $value = null;

        if (sizeof($name) > 0) {
            $value = $this->configValues;

            foreach ($name as $key) {
                if (isset($value[$key])) {
                    $value = $value[$key];
                } else {
                    $value = null;
                    break;
                }
            }
        }

        return $value;
    }
    
    /**
     * @return array
     */
    public function getAll()
    {
        return $this->configValues;
    }

    /**
     * Get Location of File Configs
     *
     * @return string
     */
    public function getConfigLocation()
    {
        return $this->location;
    }

    /**
     * Get System Environment
     *
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param string $environment
     * @return $this
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Load configuration from Yaml files
     */
    private function loadYamlConfig()
    {
        foreach ($this->getConfigLocation() as $dir) {
            $files = $this->getYamlFiles($dir);

            $this->mergeConfig($files);
        }
    }

    private function mergeConfig($files)
    {
        foreach ($files['default'] as $key => $configFile) {
            if (!isset($this->configValues[$key])) {
                $this->configValues[$key] = array();
            }

            $this->configValues[$key] = array_replace_recursive(
                $this->configValues[$key],
                Yaml::parse(
                    file_get_contents($configFile)
                )
            );

            if (!empty($files[$this->getEnvironment()][$key])) {
                $this->configValues[$key] = array_replace_recursive(
                    $this->configValues[$key],
                    Yaml::parse(
                        file_get_contents($files[$this->getEnvironment()][$key])
                    )
                );
            }
        }
    }

    /**
     * Get list of config files
     *
     * @param $dir string
     * @return array
     */
    private function getYamlFiles($dir)
    {
        $dirContent = glob($dir .'/*.yml');

        $configFiles = array(
            'default' => array()
        );
        $configFiles[$this->getEnvironment()] = array();

        if (sizeof($dirContent) > 0) {
            foreach ($dirContent as $dirEntry) {
                $ymlFile = basename($dirEntry);
                if (strpos($ymlFile, '_') === false) {
                    $k = substr($ymlFile, 0, strrpos($ymlFile, '.'));
                    // Add Default Config file
                    $configFiles['default'][$k] = $dirEntry;
                } else {
                    // Add Override Config file (for environment)
                    $_tmp = substr($ymlFile, 0, strrpos($ymlFile, '.'));
                    $_tmp = explode('_', $_tmp);

                    if ($_tmp[1] == $this->getEnvironment()) {
                        $configFiles[$this->getEnvironment()][$_tmp[0]] = $dirEntry;
                    }
                }
            }
        }

        return $configFiles;
    }
}
