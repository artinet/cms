<?php

namespace Meccano;

use Meccano\Database\Model\Article;
use Meccano\Database\Repository\ModelRepository;
use Meccano\Events;
use Meccano\Exceptions;
use Meccano\Http\Request;
use Meccano\Http\Response;
use Meccano\Listeners;
use Meccano\Routing\Tree;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Session\Session;
use Meccano\View\ViewStaticFactory;
use Meccano\View\ViewInterface;
use Meccano\Routing\Tree\TreeFactory;
use Meccano\Routing\Tree\TreeException;

/**
 * Class Cms
 *
 * @package MeccanoCMS
 * @author Novachok Oleksandr <novachok@gmail.com>
 */
class Cms
{

    const CMS_VERSION = '3.0.dev';
    const CMS_SERIAL  = '20180330';

    /**
     * @var string
     */
    private $environment;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var ContainerBuilder
     */
    private $container;

    public function __construct($environment, $rootDir)
    {
        $this->environment = $environment;
        $this->rootDir     = $rootDir;

        $this->buildContainer();
    }

    /**
     * Run Application
     *
     * @return Response
     */
    public function run()
    {
        $request  = $this->createRequest();
        $response = new Response();
        $response->prepare($request);

        try {
            $container = $this->getContainer();
            $container->set('request', $request);
            
            $this->registerSystemSubscribers();
            $this->registerCustomSubscribers();

            /**
             * Set Request in container, trigger MeccanoEvents::REQUEST event
             */
            $this->getEventDispatcher()
                ->dispatch(
                    MeccanoEvents::REQUEST, 
                    new Events\RequestEvent($container, $request, $response)
                );

            /**
             * Load section
             * Set Section in container
             */
            $section = $this->getSection($request);

            /**
             * Trigger the URL event
             */
            $this->getEventDispatcher()
                ->dispatch(
                    MeccanoEvents::getUrlEventName($section->getUrl()),
                    new Events\RequestEvent($container, $request, $response)
                );
            
            /**
             * Trigger section related events
             */
            $this->getEventDispatcher()
                ->dispatch(MeccanoEvents::SECTION_AFTER, new Events\SectionAfterEvent($container, $section));
            MeccanoEvents::dispatchSectionEvents($this->getEventDispatcher(), $section);

            /**
             * Load article or if it is impossible - a list of articles
             * @var Article $article
             */
            $article = $this->getArticle($request);
            if (!empty($article)) {
                $this->getEventDispatcher()
                    ->dispatch(MeccanoEvents::ARTICLE_AFTER, new Events\ArticleEvent($article));
            } else {
                $this->getArticles();
            }

            $this->getEventDispatcher()
                ->dispatch(MeccanoEvents::RESPONSE, new Events\ResponseEvent($container, $response));

        } catch (\Exception $e) {
            $this->getEventDispatcher()
                ->dispatch(
                    MeccanoEvents::RESPONSE_ERROR,
                    new Events\ErrorEvent($request, $response, $this->getContainer()->get('data'), $e)
                );
        }
        
        return $this->fillContentToResponse($request, $response);
    }

    /**
     * Create Request
     * @return Request
     */
    private function createRequest()
    {
        $request = Request::createFromGlobals();
        $session = new Session();
        $session->start();
        $request->setSession($session);

        return $request;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return \Meccano\Http\Response
     */
    private function fillContentToResponse(Request $request, Response $response)
    {
        $response->headers->set('Content-Type', $request->getAcceptableContentTypes());
        /** @var ViewInterface $view */
        $view = $this->getContainer()->get('view');
        $response->setContent($view->getContent());

        return $response;
    }

    /**
     * Get System environment
     *
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * Get Config Dir
     *
     * @return string
     */
    public function getConfigDirs()
    {
        $dirs = array(
            $this->rootDir .'/config'
        );

        return $dirs;
    }

    /**
     * Get Web root dir
     *
     * @return string
     */
    public function getWebRoot()
    {
        return $this->rootDir .'/public';
    }

    /**
     * Get Log dir
     *
     * @return string
     */
    public function getLogDir()
    {
        return $this->rootDir .'/var/log';
    }

    /**
     * Get Cache dir
     *
     * @return string
     */
    public function getCacheDir()
    {
        return $this->rootDir .'/var/cache';
    }

    /**
     * check is in debug mode
     *
     * @return bool
     */
    public function isDebug()
    {
        return defined('DEBUG') ? DEBUG : false;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->container->get('config');
    }

    /**
     * @return EventDispatcher
     */
    public function getEventDispatcher()
    {
        return $this->container->get('events');
    }

    /**
     * Register Subscribers for events
     */
    protected function registerSystemSubscribers()
    {
        $eventDispatcher = $this->getEventDispatcher();

        $eventDispatcher->addSubscriber(new Listeners\LocationMatcherListener());

        /** @var Config $config */
        $config = $this->getConfig();

        if ($this->isDebug() && $config->get('app.dev_toolbar')) {
            $eventDispatcher->addSubscriber(new Listeners\DevToolbarListener());
        }

        // Security Subscriber
        $eventDispatcher->addSubscriber(new Listeners\SecurityListener());

        // Error Subscriber
        $eventDispatcher->addSubscriber(new Listeners\ErrorListener());
    }

    protected function registerCustomSubscribers()
    {
        $events_dir = $this->getConfig()->get('app.path.root') . '/events';
        // TODO: load custom subscribers
    }

    /**
     * Build system dependency injection container
     */
    private function buildContainer()
    {
        $container = new ContainerBuilder();

        $container->setParameter('path.root', $this->rootDir);
        $container->setParameter('path.config', $this->getConfigDirs());
        $container->setParameter('path.webroot', $this->getWebRoot());
        $container->setParameter('path.log', $this->getLogDir());
        $container->setParameter('path.cache', $this->getCacheDir());
        $container->setParameter('app.environment', $this->getEnvironment());
        $container->setParameter('debug', $this->isDebug());

        $container->set('meccano', $this);

        $container->register('events', 'Symfony\Component\EventDispatcher\EventDispatcher');
        $container->register('data', 'Meccano\View\DataContainer');
        $container->register('config', 'Meccano\Config')
            ->setArguments(array(
                '%path.config%',
                '%app.environment%',
                $container
            ));
            
        $container->register('view')
            ->setArguments([
                new Reference('request'),
                new Reference('config'),
                new Reference('data'),
                // '%path.root%'
            ])
            ->setFactory([
                ViewStaticFactory::class,
                'createViewManager'
            ]);

        $dbDefinition = new Definition(
            'Meccano\Database\Db',
            array(new Reference('config'))
        );
        $dbDefinition->setFactory(array(
            'Meccano\Database\DbFactory',
            'createDbService'
        ));
        $container->setDefinition('database', $dbDefinition);

        $logDefinition = new Definition(
            'Monolog\Logger',
            array(new Reference('config'), '%path.log%', '%debug%')
        );
        $logDefinition->setFactory(array(
            'Meccano\LogFactory',
            'createLogService'
        ));
        $container->setDefinition('log', $logDefinition);

        $this->container = $container;
    }

    /**
     * Helper getters
     */

    /**
     * @return Logger
     * @throws Exceptions\Error500Exception
     */
    public function getLogger()
    {
        $logger = $this->container
            ->get('log');

        if (!$logger) {
            throw new Exceptions\Error500Exception("Can't get a logger");
        }

        return $logger;
    }


    /**
     * Get current section
     *
     * @param Request $request
     * @return Tree\Node|null
     * @throws Exceptions\Error404Exception
     */
    private function getSection(Request $request)
    {
        $path = $request->getWorkingPath();

        try {
            // Build Tree
            $tree = TreeFactory::build($this->getContainer(), $path[0]);
            
        } catch (TreeException $e) {
            throw new Exceptions\Error404Exception();
        }
        
        /** @var Tree\Tree $tree */
        $this->getContainer()->set('tree', $tree);

        /** @var Tree\Node $root */
        $root = $tree->first();
        $node = null;

        // Reset Root node in case of some prefix path, for example: /admin
        if (!empty($path) && $path[0] === $root->alias) {
            unset($path[0]);
        }
        $node = $root;
        
        $sections = $root->getChildren();
        $_tmp = null;
        
        foreach ($path as $el) {
            $_tmp = $sections->findByAlias($el);
            
            if (!empty($_tmp)) {
                $node = $_tmp;
            } elseif (empty($node)) {
                throw new Exceptions\Error404Exception();
            } else {
                break;
            }
        }
    
        $data = $this->getContainer()->get('data');
        $data['section'] = $node;
        $data['tree'] = $tree;

        // TODO: create listener to redirect if forward-in
        return $node;
    }

    /**
     * Get Article by hanging point of request URL (after section is calculated)
     *
     * @param Request $request
     * @return Database\Model\ModelInterface|null
     * @throws Exceptions\Error404Exception
     */
    private function getArticle(Request $request)
    {
        $section       = $this->getContainer()->get('data')['section'];
        $point         = null;
        $path_elements = $request->getWorkingPath();

        for ($i=0, $t=sizeof($path_elements); $i<$t; $i++) {
            $element = $path_elements[$i];
            if ($element == $section->id || $element == $section->alias) {
                $next = $i + 1;
                $point = isset($path_elements[$next]) ? $path_elements[$next] : null;
                break;
            }
        }

        if (empty($point)) {
            return null;
        }

        /** @var ModelRepository $repository */
        $repository = $this->getContainer()->get('database')->model('Article');
        $repository
            ->where('is_active', 1, '=');

        if (is_numeric($point)) {
            $repository->andWhere('id', $point, '=');
        } else {
            $repository->andWhere('alias', $point, '=');
        }

        $article = $repository->getOne();

        if (empty($article)) {
            throw new Exceptions\Error404Exception();
        }

        $data = $this->getContainer()->get('data');
        $data['article'] = $article;

        return $article;
    }

    /**
     * Get list of articles
     */
    public function getArticles()
    {
        $section = $this->getContainer()->get('data')['section'];

        /** @var ModelRepository $repository */
        $repository = $this->getContainer()->get('database')->model('Article');

        $articles = $repository
            ->where('is_active', 1, '=')
            ->andWhere('section_id', $section->id, '=')
            ->order('position', 'asc')
            ->get();

        $data = $this->getContainer()->get('data');
        $data['articles'] = $articles;
    }
}
