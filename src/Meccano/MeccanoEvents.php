<?php

namespace Meccano;

use Meccano\Events\SectionNodeEvent;
use Meccano\Events\SectionNodeGlobalEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Meccano\Routing\Tree\NodeInterface;

class MeccanoEvents
{
    const REQUEST             = 'request';
    const RESPONSE            = 'response';
    const RESPONSE_ERROR      = 'response.error';
    const SECTION_BEFORE      = 'section.before';
    const SECTION_AFTER       = 'section.after';
    const ARTICLE_BEFORE      = 'article.before';
    const ARTICLE_AFTER       = 'article.after';
    const ARTICLES_BEFORE     = 'articles.before';
    const ARTICLES_AFTER      = 'articles.after';
    const TMPL_REQUEST_URL    = 'request.url.%s';
    const TMPL_NODE           = 'section.id.%d';
    const TMPL_NODE_GLOBAL    = 'section.id.%d.global';
    
    public static function getUrlEventName($path)
    {
        return sprintf(self::TMPL_REQUEST_URL, (string)$path);
    }

    /**
     * Get event name on node loaded
     *
     * @param NodeInterface $node
     * @return string
     */
    public static function getSectionEventName(NodeInterface $node)
    {
        return sprintf(self::TMPL_NODE, (int)$node->getId());
    }

    /**
     * Get event name for global event on node loaded
     *
     * @param NodeInterface $node
     * @return string
     */
    public static function getSectionEventGlobalName(NodeInterface $node)
    {
        return sprintf(self::TMPL_NODE_GLOBAL, (int)$node->getId());
    }

    /**
     * Dispatch events on node loaded
     *
     * @param EventDispatcherInterface $dispatcher
     * @param NodeInterface $node
     */
    public static function dispatchSectionEvents(EventDispatcherInterface $dispatcher, NodeInterface $node)
    {
        $originalNode = $node;
        $dispatcher->dispatch(self::getSectionEventName($node), new SectionNodeEvent($node));

        while (($node = $node->getParent()) instanceof NodeInterface) {
            $dispatcher->dispatch(
                self::getSectionEventGlobalName($node),
                new SectionNodeGlobalEvent($originalNode, $node)
            );
        }
    }
}
