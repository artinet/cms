<?php
namespace Meccano\View;


class HtmlView extends ViewAbstract implements ViewInterface
{    
    /**
     * {@inheritDoc}
     * @see ViewInterface::getContent()
     */
    public function getContent()
    {
        $template = $this->defineTemplate();
        // TODO: define META values
        
        return $this->engine->render(
            $template,
            $this->data
        );
    }
    
    /**
     * Define template name
     *
     * @return null|string
     */
    private function defineTemplate()
    {
        $template      = $this->data->getTemplate();
        $template_type = 'base';
        
        if (!$template) {
            // Get all extending templates
            $ext_templates = glob(
                $this->engine->getTemplateDir() .'/ext/*.twig'
            );
            $ext_templates = array_map(function ($path) {
                return substr($path, strpos($path, 'ext'));
            }, $ext_templates);
                
                // Group ext templates
                $t    = sizeof($ext_templates);
                $_tmp = array();
                for ($i=0; $i<$t; $i++) {
                    preg_match_all('/(\d+)\S+(article|articles|base)/', $ext_templates[$i], $m, PREG_SET_ORDER);
                    if (!empty($m[0])) {
                        if (!isset($_tmp[$m[0][1]])) {
                            $_tmp[$m[0][1]] = array();
                        }
                        
                        $_tmp[$m[0][1]][$m[0][2]] = $ext_templates[$i];
                    }
                }
                $ext_templates = $_tmp;
                unset($_tmp);
                
                $article = $this->data['article'];
                $articles = $this->data['articles'];
                
                if (!empty($article)) {
                    $template_type = 'article';
                } elseif (sizeof($articles) > 1) {
                    $template_type = 'articles';
                } elseif (sizeof($articles) == 1) {
                    $this->data['article'] = $articles->first();
                    $template_type = 'article';
                }
                
                /** @var Node $section */
                $section = $this->data['section'];
                if (isset($ext_templates[$section->id][$template_type])) {
                    $template = $ext_templates[$section->id][$template_type];
                } else {
                    while (($section = $section->getParent())) {
                        if (isset($ext_templates[$section->id][$template_type])) {
                            $template = $ext_templates[$section->id][$template_type];
                            break;
                        }
                    }
                }
        }
        
        
        if (!$template) {
            $template = $template_type .'.html.twig';
        }
        
        if (!empty($template) && strpos($template, '.twig') === false) {
            $template .= '.twig';
        }
        
        return $template;
    }
}
