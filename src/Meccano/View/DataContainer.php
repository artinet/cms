<?php

namespace Meccano\View;

class DataContainer implements \ArrayAccess, \JsonSerializable
{
    private $data = [];
    
    private $content = null;

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }

    /**
     * Get template name
     *
     * @return string|null
     */
    public function getTemplate()
    {
        return isset($this->data['template']) ? $this->data['template'] : null;
    }

    /**
     * Get Data as array from object
     *
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }
    
    /**
     * Set the main content of DTO
     * 
     * @param string $name
     * @param mixed $content
     */
    public function setContent($name, $content)
    {
        $this->content = $content;
        $this->offsetSet($name, $content);
    }
    
    /**
     * Get the main content of DTO
     * 
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }
}
