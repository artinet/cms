<?php
namespace Meccano\View;

interface TemplateEngineInterface
{
    /**
     * Get Template storage directory
     * @return string
     */
    public function getTemplateDir();
    
    /**
     * Render Template
     * @param string $template
     * @param DataContainer $data
     * @return string
     */
    public function render($template, DataContainer $data);
}
