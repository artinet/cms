<?php
namespace Meccano\View;


abstract class ViewAbstract implements ViewInterface
{
    /** @var TemplateEngineInterface */
    protected $engine;

    /** @var DataContainer $data */
    protected $data;
    
    public function __construct(TemplateEngineInterface $engine)
    {
        $this->engine = $engine;
    }

    
    /**
     * {@inheritDoc}
     * @see \Meccano\View\ViewInterface::setData()
     */
    public function setData(DataContainer $data)
    {
        $this->data = $data;
        
        return $this;
    }
}
