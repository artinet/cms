<?php
namespace Meccano\View;


class JsonView extends ViewAbstract implements ViewInterface
{
    public function getContent()
    {
        return $this->engine->render(
            '',
            $this->data
        );
    }
}
