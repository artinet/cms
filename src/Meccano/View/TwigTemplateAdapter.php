<?php
namespace Meccano\View;


class TwigTemplateAdapter implements TemplateEngineInterface
{
    private $template_location;
    
    private $cache_location;
    
    private $engine;
    
    public function __construct($templates_dir, $cache_dir) 
    {
        $this->template_location = $templates_dir;
        $this->cache_location = $cache_dir;

        $this->initEngine();
    }
    
    private function initEngine() 
    {
        if (!file_exists($this->cache_location)) {
            mkdir($this->cache_location, 0777, true);
        }

        $loader = new \Twig_Loader_Filesystem($this->template_location);
        $this->engine = new \Twig_Environment($loader, array(
            'cache' => $this->cache_location,
            'debug' => $this->isDebug()
        ));
    }
    
    /**
     * Is Debug mode?
     * @return string|boolean
     */
    private function isDebug()
    {
        return defined('DEBUG') ? DEBUG : false;
    }
    
    /**
     * {@inheritDoc}
     * @see \Meccano\View\TemplateEngineInterface::getTemplateDir()
     */
    public function getTemplateDir()
    {
        return $this->template_location;
    }
    
    /**
     * {@inheritDoc}
     * @see \Meccano\View\TemplateEngineInterface::render()
     */
    public function render($template, DataContainer $data)
    {
        return $this->engine->render(
            $template,
            $data->toArray()
        );
    }
}
