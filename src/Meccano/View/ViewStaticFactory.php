<?php
namespace Meccano\View;


use Meccano\Config;
use Meccano\Http\Request;
use Meccano\Exceptions\Error415Exception;
use Meccano\Exceptions\Error500Exception;

class ViewStaticFactory
{
    public static function createViewManager(Request $request, Config $config, DataContainer $data)
    {
        $requested_type = $request->getAcceptableType();
        $allowed_type = $config->get('app.content_type');
        
        if($allowed_type && $allowed_type !== $requested_type) {
            throw new Error415Exception();
        }
        
        $cache_dir    = $config->get('app.path.root') . $config->get('app.cache') .'/view';
        $template_dir = $config->get('app.path.root') . $config->get('app.templates');
        
        switch ($requested_type) {
            case 'html':
                $view = new HtmlView(new TwigTemplateAdapter($template_dir, $cache_dir));
                break;
                
            case 'json':
                $view = new JsonView(new JsonTemplateAdapter());
                break;
                
            case 'xml':
                $view = new XmlView(new TwigTemplateAdapter($template_dir, $cache_dir));
                break;
        }
        
        if(!$view) {
            throw new Error500Exception("Can't build View Manager");
        }
        
        $view->setData($data);
        
        return $view;
    }
}
