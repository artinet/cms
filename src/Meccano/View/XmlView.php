<?php
namespace Meccano\View;


class XmlView extends ViewAbstract implements ViewInterface
{
    public function getContent()
    {
        return <<<XML
<?xml version="1.0"?>
<page>
    <content>
        Support of XML not implemented yet.
    </content>
</page>
XML;
    }
}
