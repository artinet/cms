<?php
namespace Meccano\View;

class JsonTemplateAdapter implements TemplateEngineInterface
{
    /**
     * @see TemplateEngineInterface::getTemplateDir()
     */
    public function getTemplateDir()
    {
        return '';
    }

    /**
     * @see TemplateEngineInterface::render()
     */
    public function render($template, DataContainer $data)
    {
        return json_encode([
            'data' => $data->getContent()
        ]);
    }
}
