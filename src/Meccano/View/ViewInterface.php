<?php
namespace Meccano\View;


interface ViewInterface
{
    public function setData(DataContainer $data);
    public function getContent();
}
