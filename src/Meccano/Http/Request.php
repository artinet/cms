<?php

namespace Meccano\Http;

use Meccano\Database\Model\Language;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\ParameterBag;

class Request extends SymfonyRequest
{
    const CONTENT_EXT_HTML = 'html';
    const CONTENT_EXT_JSON = 'json';
    const CONTENT_EXT_XML  = 'xml';
    
    /** @var array */
    protected $workingPath;

    /** @var Language */
    protected $defaultLanguage;

    /** @var Language */
    protected $currentLanguage;
    
    public static function createFromGlobals()
    {
        $request = parent::createFromGlobals();
        
        if ($request->headers->get('content-type') == 'application/json') {
            $data = json_decode($request->getContent(), true);
            if ($data) {
                $request->request = new ParameterBag($data);
            }
        }
        
        return $request;
    }

    /**
     * @return array
     */
    public function getWorkingPath()
    {
        if (!isset($this->workingPath)) {
            $path = $this->getPathInfo();
            $path = explode('/', $path);
            $path = array_filter(
                $path,
                function ($v) {
                    return !empty($v);
                }
            );
            $path = array_map(
                function ($v) {
                    $v = strrpos($v, '.') !== false ? substr($v, 0, strrpos($v, '.')) : $v;
                    $v = strtolower($v);
                    return $v;
                },
                $path
            );

            $this->workingPath = array_values($path);
            
            if (empty($this->workingPath)) {
                $this->workingPath = ['index'];
            }
        }

        return $this->workingPath;
    }


    /**
     * @param $part string
     * @return $this
     */
    public function excludeFromPath($part)
    {
        if (!empty($this->workingPath)) {
            $i = array_search($part, $this->workingPath);
            if ($i !== false) {
                unset($this->workingPath[$i]);
                $this->workingPath = array_values($this->workingPath);
            }
        }

        return $this;
    }

    /**
     * Set default language and default locale
     *
     * @param Language $language
     * @return $this
     */
    public function setDefaultLanguage(Language $language)
    {
        $this->setDefaultLocale($language->alias);

        $this->defaultLanguage = $language;

        return $this;
    }

    /**
     * Get default language
     *
     * @return Language
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * Set current language and current locale
     *
     * @param Language $language
     * @return $this
     */
    public function setCurrentLanguage(Language $language)
    {
        $this->setLocale($language->alias);

        $this->currentLanguage = $language;

        return $this;
    }

    /**
     * Get current language
     *
     * @return Language
     */
    public function getCurrentLanguage()
    {
        return $this->currentLanguage;
    }

    /**
     * Get the acceptable type of response
     * 
     * @return string
     */
    public function getAcceptableType()
    {
        $ext = strrpos($this->getPathInfo(), '.') !== false ?
            substr($this->getPathInfo(), strrpos($this->getPathInfo(), '.')+1) :
            null;
        $ext = strtolower($ext);

        if ($ext == 'html' || $ext == 'htm') {
            return self::CONTENT_EXT_HTML;
        }

        if ($ext == 'json') {
            return self::CONTENT_EXT_JSON;
        }
        
        if ($ext == 'xml') {
            return self::CONTENT_EXT_XML;
        }

        if (in_array('text/html', $this->getAcceptableContentTypes())) {
            return self::CONTENT_EXT_HTML;
        }

        if (in_array('application/json', $this->getAcceptableContentTypes())) {
            return self::CONTENT_EXT_JSON;
        }
        
        if (in_array('application/xml', $this->getAcceptableContentTypes())) {
            return self::CONTENT_EXT_XML;
        }

        return self::CONTENT_EXT_HTML;
    }
}
