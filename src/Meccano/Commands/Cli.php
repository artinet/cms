<?php

namespace Meccano\Commands;

use Meccano\Cms;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class Cli extends Application
{
    /** @var Cms */
    private $cms;

    public function __construct(Cms $cms)
    {
        $this->cms = $cms;

        parent::__construct(
            'meccano',
            Cms::CMS_VERSION .'-'. $cms->getEnvironment() . ($cms->isDebug() ? '/debug' : '')
        );

        $this->getDefinition()
            ->addOption(
                new InputOption(
                    '--env',
                    null,
                    InputOption::VALUE_REQUIRED,
                    'The Environment name.',
                    $this->cms->getEnvironment()
                )
            );
    }

    /**
     * @inheritdoc
     */
    public function add(Command $command)
    {
        $command = parent::add($command);
        if ($command instanceof ContainerAwareInterface) {
            $command->setContainer($this->cms->getContainer());
        }

        return $command;
    }

    /**
     * Batch load of the command from config file
     */
    public function batchLoad()
    {
        $commands = $this->cms
            ->getContainer()
            ->get('config')
            ->get('commands');

        if (sizeof($commands) > 0) {
            foreach ($commands as $command) {
                $commandInstance = new $command();
                $this->add($commandInstance);
            }
        }
    }
}
