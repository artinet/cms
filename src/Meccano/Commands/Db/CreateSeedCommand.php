<?php

namespace Meccano\Commands\Db;

use Phinx\Console\Command\SeedCreate;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class CreateSeedCommand extends SeedCreate implements ContainerAwareInterface
{
    use PhinxHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('db:seed:create');
    }
}
