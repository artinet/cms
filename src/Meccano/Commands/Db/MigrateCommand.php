<?php

namespace Meccano\Commands\Db;

use Phinx\Console\Command\Migrate;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class MigrateCommand extends Migrate implements ContainerAwareInterface
{
    use PhinxHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('db:migrate');
    }
}
