<?php

namespace Meccano\Commands\Db;

use Phinx\Console\Command\Create;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class CreateMigrationCommand
 *
 * Proxy command to the Phinx command: create
 *
 * @package Meccano\Commands\Db
 */
class CreateMigrationCommand extends Create implements ContainerAwareInterface
{
    use PhinxHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('db:migration:create');
    }
}
