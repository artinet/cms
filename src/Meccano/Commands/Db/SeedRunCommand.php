<?php

namespace Meccano\Commands\Db;

use Phinx\Console\Command\SeedRun;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class SeedRunCommand extends SeedRun implements ContainerAwareInterface
{
    use PhinxHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('db:seed:run');
    }
}
