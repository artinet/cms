<?php

namespace Meccano\Commands\Db;

use Phinx\Console\Command\Rollback;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class RollbackCommand extends Rollback implements ContainerAwareInterface
{
    use PhinxHelper;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('db:rollback');
    }
}
