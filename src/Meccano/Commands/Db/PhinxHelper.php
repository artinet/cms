<?php

namespace Meccano\Commands\Db;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PhinxProxy
 *
 * Class as a proxy to run Phinx commands
 *
 * @package Meccano\Commands\Db
 */
trait PhinxHelper
{

    protected $container;

    /**
     * Set Container
     *
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Get path to the Phinx config file
     *
     * @return string
     */
    protected function getPhinxConfigPath()
    {
        $phinxConfigFilename = $this->container->get('config')->get('database.phinx.config');
        $configDirs          = $this->container->getParameter('path.config');
        $configDirs          = array_reverse($configDirs);

        foreach ($configDirs as $dir) {
            if (file_exists($dir .'/'. $phinxConfigFilename)) {
                return $dir .'/'. $phinxConfigFilename;
            }
        }

        return null;
    }

    protected function updateOptions()
    {
        $this->getDefinition()
            ->getOption('configuration')
            ->setDefault($this->getPhinxConfigPath());

        if ($this->getDefinition()->hasShortcut('e')) {
            $this->getDefinition()
                ->getOptionForShortcut('e')
                ->setDefault($this->getDefinition()->getOption('env')->getDefault());
        }
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->updateOptions();
        parent::execute($input, $output);
    }
}
