<?php

namespace Meccano\Routing;

use Meccano\Database\DataCollection;
use Meccano\Database\Model\Language;
use Meccano\Http\Request;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LocationMatcher implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var  DataCollection */
    private $locales;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function match(Request $request)
    {
        $this->loadLocales();

        $workingPath  = $request->getWorkingPath();
        $firstElement = reset($workingPath);
        $host = $request->getHost();

        /** @var Language $currentLanguage */
        $currentLanguage = $this->locales->find(function ($i, Language $language) use ($firstElement, $host) {
            if ($language->alias == $firstElement || $language->domain == $host) {
                return true;
            }
            return false;
        });

        // Set Current Language and locale
        if ($currentLanguage) {
            $request
                ->setCurrentLanguage($currentLanguage)
                ->excludeFromPath($firstElement);
        }



        /** @var Language $defaultLanguage */
        $defaultLanguage = $this->locales->find(function ($i, Language $language) {
            if ($language->is_default) {
                return true;
            }
            return false;
        });

        if ($defaultLanguage) {
            $request->setDefaultLanguage($defaultLanguage);

            if (!$currentLanguage) {
                $request
                    ->setCurrentLanguage($defaultLanguage);
            }
        }

        return $request;
    }

    private function loadLocales()
    {
        $this->locales = $this->container
            ->get('database')
            ->model('Language')
            ->all();
    }
}
