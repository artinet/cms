<?php

namespace Meccano\Routing\Tree;

use Meccano\Database\DataCollection;

class Tree extends DataCollection implements TreeInterface
{
    /**
     * Get Node from first level by its Alias
     *
     * @param $alias string
     * @return Node|null
     */
    public function findByAlias($alias)
    {
        if (sizeof($this) > 0) {
            /** @var Node $node */
            foreach ($this as $node) {
                if ($node->alias == $alias) {
                    return $node;
                }
            }
        }

        return null;
    }

    /**
     * Get Node from first level by its ID
     *
     * @param $id integer
     * @return Node|null
     */
    public function findById($id)
    {
        if (sizeof($this) > 0) {
            /** @var Node $node */
            foreach ($this as $node) {
                if ($node->id == $id) {
                    return $node;
                }
            }
        }

        return null;
    }
}
