<?php

namespace Meccano\Routing\Tree;

interface TreeInterface
{
    public function findByAlias($alias);
    public function findById($id);
}
