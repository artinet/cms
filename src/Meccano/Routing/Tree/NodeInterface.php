<?php

namespace Meccano\Routing\Tree;

use Meccano\Database\Model\SectionInterface;

interface NodeInterface extends SectionInterface
{   
    public function __construct(SectionInterface $section);

    /**
     * Get Section
     * @return SectionInterface
     */
    public function getSection();
    
    /**
     * Set Children Tree
     * @param TreeInterface $tree
     */
    public function setChildren(TreeInterface $tree);
    
    /**
     * Get Children Tree
     * @return TreeInterface
     */
    public function getChildren();
    
    /**
     * Set the Parent Node
     * @param NodeInterface $node
     */
    public function setParent(NodeInterface $node = null);
    
    /**
     * Get the Parent Node
     * @return NodeInterface
     */
    public function getParent();
    
    /**
     * Get the URL for a Node
     * @return string
     */
    public function getUrl();
}
