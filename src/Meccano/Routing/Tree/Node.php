<?php

namespace Meccano\Routing\Tree;

use Meccano\Database\DataCollection;
use Meccano\Database\Model\SectionInterface;

/**
 * Class Node
 * @package Meccano\Routing\Tree
 * @property $id
 * @property $alias
 */
class Node implements NodeInterface, \JsonSerializable
{
    /** @var SectionInterface */
    private $section;

    /** @var NodeInterface */
    private $parent;

    /** @var TreeInterface */
    private $children;

    private $data = array();

    /**
     * TreeItem constructor.
     *
     * @param SectionInterface $section
     */
    public function __construct(SectionInterface $section)
    {
        $this->section  = $section;
        $this->children = new Tree();
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getId()
     */
    public function getId()
    {
        return $this->section->getId();
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getAlias()
     */
    public function getAlias()
    {
        return $this->section->getAlias();
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getLt()
     */
    public function getLt()
    {
        return $this->section->getLt();
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getRt()
     */
    public function getRt()
    {
        return $this->section->getRt();
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getLevel()
     */
    public function getLevel()
    {
        return $this->section->getLevel();
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        if (method_exists($this, 'get'. ucfirst($name))) {
            $method = 'get'. ucfirst($name);

            return $this->{$method}();
        }

        return $this->section->{$name};
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::getSection()
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::setChildren()
     */
    public function setChildren(TreeInterface $tree)
    {
        $this->children = $tree;

        return $this;
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::getChildren()
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::setParent()
     */
    public function setParent(NodeInterface $node = null)
    {
        $this->parent = $node;

        return $this;
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::getParent()
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * {@inheritDoc}
     * @see NodeInterface::getUrl()
     */
    public function getUrl()
    {
        if (!isset($this->data['url'])) {
            $url = $this->section->alias;

            if ($this->getParent()) {
                $url = $this->getParent()->getUrl() . '/'. $url;
            } else {
                $url = '/'. $url;
            }

            $this->data['url'] = $url;
        }

        return $this->data['url'];
    }

    /**
     * {@inheritDoc}
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize()
    {
        $section = $this->getSection();
        $section['children'] = $this->getChildren();
        return $section;
    }
}
