<?php

namespace Meccano\Routing\Tree;

use Meccano\Database\DataCollection;
use Meccano\Database\Model\Section;
use Meccano\Database\Repository\ModelRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TreeFactory
{

    /**
     * Build Tree of sections by given root ID
     *
     * @param ContainerInterface $container
     * @param $rootId
     * @return Tree
     * @throws TreeException
     */
    public static function build(ContainerInterface $container, $alias)
    {
        /** @var ModelRepository $sectionRepository */
        $sectionRepository = $container->get('database')->model('Section');

        /** @var Section $rootNode */
        $rootNode = $sectionRepository->where('alias', $alias)
            ->getOne();

        if (!$rootNode) {
            throw new TreeException();
        }

        $sections = $sectionRepository
            ->where('lt', $rootNode->lt, '>=')
            ->andWhere('rt', $rootNode->rt, '<=')
            ->order('lt')
            ->get();

        return self::buildTree($sections, null);
    }

    /**
     * Build Tree
     *
     * @param DataCollection $sections
     * @param NodeInterface|null $parentNode
     * @return Tree
     */
    private static function buildTree(DataCollection $sections, NodeInterface $parentNode = null)
    {
        /** @var Node $node */
        $node = null;
        $tree = new Tree();

        while ($sections->count() > 0) {
            /** @var Section $section */
            $section = $sections->first();

            if (!empty($node) && $node->level < $section->level) {
                $node->setChildren(self::buildTree($sections, $node));
            }

            if ($sections->count() == 0) {
                continue;
            }

            /** @var Section $section */
            $section = $sections->first();

            if (!empty($node) && $node->level > $section->level) {
                break;
            }

            $node = new Node($section);
            $node->setParent($parentNode);
            $sections->removeByItem($section);

            $tree->add($node);
        }

        return $tree;
    }
}
