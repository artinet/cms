<?php

namespace Meccano\Routing;

class RouteCollection implements \IteratorAggregate, \Countable
{
    private $routes = array();

    /**
     * Get size of routes
     *
     * @return int
     */
    public function count()
    {
        return sizeof($this->routes);
    }

    /**
     * Iterate routes
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->routes);
    }

    public function add(RouteInterface $route)
    {
        $this->routes[] = $route;
    }
}
