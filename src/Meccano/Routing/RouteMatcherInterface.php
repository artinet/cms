<?php

namespace Meccano\Routing;

use Meccano\Http\Request;

interface RouteMatcherInterface
{
    public function matchRequest(Request $request);
}
