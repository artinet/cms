<?php

namespace Meccano\Database;

use Meccano\Database\Model\ModelException;
use Meccano\Database\Repository\RepositoryException;
use Meccano\Database\Repository\RepositoryInterface;

/**
 * Class Db
 *
 * @package Meccano\Database
 */
class Db
{

    /**
     * @var string
     */
    private $table_prefix = '';

    /** @var \PDO */
    private $connection;

    public function __construct($dsn, $username, $password, $options = null)
    {
        $this->connection = new \PDO($dsn, $username, $password, $options);
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Set prefix of the tables
     * @param $prefix
     * @return $this
     */
    public function setTablePrefix($prefix)
    {
        $this->table_prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix of the tables
     * @return string
     */
    public function getTablePrefix()
    {
        return $this->table_prefix;
    }

    /**
     * Get repository of model
     *
     * @param $model
     * @return RepositoryInterface
     * @throws ModelException
     * @throws RepositoryException
     */
    public function model($model)
    {
        if (is_string($model)) {
            if (strpos($model, 'Model') === false) {
                $model = $this->getNamespace() . '\Model\\'. $model;
            }

            $modelClassReflection = new \ReflectionClass($model);
            $model = $modelClassReflection->newInstance();
        } elseif (is_object($model)) {
            $modelClassReflection = new \ReflectionClass($model);
            if (!$modelClassReflection->implementsInterface($this->getNamespace() .'\ModelInterface')) {
                throw new ModelException();
            }
        }

        $repositoryClass = $this->getNamespace() .
            '\Repository\\'.
            $modelClassReflection->getShortName() .
            'Repository';

        if (class_exists($repositoryClass)) {
            $repositoryReflection = new \ReflectionClass($repositoryClass);
        } else {
            $repositoryReflection = new \ReflectionClass($this->getNamespace() .'\Repository\ModelRepository');
        }

        if (!$repositoryReflection->implementsInterface($this->getNamespace() .'\Repository\RepositoryInterface')) {
            throw new RepositoryException();
        }

        /** @var RepositoryInterface $repository */
        $repository = $repositoryReflection->newInstance();
        $repository
            ->setConnection($this)
            ->setModel($model);

        return $repository;
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     */
    public function query($sql, $params = array())
    {
        $st = $this->connection->prepare($sql);
        $st->execute($params);

        return $st->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Get current namespace
     * @return string
     */
    private function getNamespace()
    {
        $reflectionThis = new \ReflectionClass($this);
        return $reflectionThis->getNamespaceName();
    }
}
