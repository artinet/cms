<?php

namespace Meccano\Database\Model;

/**
 * Class Section
 *
 * @property int $lt
 * @property int $rt
 * @property int $level
 * @property string $alias
 * @package Meccano\Database\Model
 */
class Section extends AbstractTranslableModel implements ModelInterface, TranslableModelInterface, SectionInterface
{
    public function getTable()
    {
        return 'sections';
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getId()
     */
    public function getId()
    {
        return $this->__get('id');
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getLt()
     */
    public function getLt()
    {
        return $this->__get('lt');
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getRt()
     */
    public function getRt()
    {
        return $this->__get('rt');
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getLevel()
     */
    public function getLevel()
    {
        return $this->__get('level');
    }
    
    /**
     * {@inheritDoc}
     * @see SectionInterface::getAlias()
     */
    public function getAlias()
    {
        return $this->__get('alias');
    }
}
