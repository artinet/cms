<?php

namespace Meccano\Database\Model;

abstract class AbstractTranslableModel extends AbstractModel
{
    protected $locales;

    public function getTableTranslate()
    {
        return $this->getTable() .'_i18n';
    }

    public function setLocales($locales)
    {
        $this->locales = $locales;
    }
}
