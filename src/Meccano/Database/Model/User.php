<?php
namespace Meccano\Database\Model;


use Meccano\Security\Users\UserInterface;
use Meccano\Security\Roles;

class User extends AbstractModel implements ModelInterface, UserInterface
{
    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();
        unset($data['pwd'], $data['hash']);
        return $data;
    }

    /**
     * {@inheritDoc}
     * @see ModelInterface::getTable()
     */
    public function getTable()
    {
        return 'users';
    }
    
    /**
     * {@inheritDoc}
     * @see UserInterface::getId()
     */
    public function getId()
    {
        return $this->__get('id');
    }

    /**
     * {@inheritDoc}
     * @see UserInterface::getUsername()
     */
    public function getUsername()
    {
        return $this->__get('username');
    }

    /**
     * {@inheritDoc}
     * @see UserInterface::getPassword()
     */
    public function getPassword()
    {
        return $this->__get('pwd');
    }

    /**
     * {@inheritDoc}
     * @see UserInterface::isActive()
     */
    public function isActive()
    {
        return $this->__get('is_active');
    }

    /**
     * {@inheritDoc}
     * @see UserInterface::getRoles()
     */
    public function getRoles()
    {
        return [Roles::ROLE_ADMIN];
    }
}
