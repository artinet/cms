<?php

namespace Meccano\Database\Model;

interface ModelInterface
{
    /**
     * Gate Table name
     * @return string
     */
    public function getTable();
    
    /**
     * Convert Array into Model
     * @param array $data
     * @return ModelInterface
     */
    public function fromArray(array $data);
    
    /**
     * Magic getter
     * @param string $name
     * @return mixed
     */
    public function __get($name);
    
    /**
     * Magic setter
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value);
}
