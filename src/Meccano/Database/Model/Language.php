<?php

namespace Meccano\Database\Model;

class Language extends AbstractModel implements ModelInterface
{
    /**
     * @return string
     */
    public function getTable()
    {
        return 'languages';
    }
}
