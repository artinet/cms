<?php

namespace Meccano\Database\Model;

abstract class AbstractModel implements \ArrayAccess, \JsonSerializable
{
    /**
     * @var array
     */
    protected $data = array();

    /**
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        return $this->offsetSet($name, $value);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * @inheritdoc
     * @return $this
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->data;
    }
    
    /**
     * Set Object's data from array
     *
     * @param array $data
     * @return $this
     */
    public function fromArray(array $data)
    {
        $this->data = array_replace_recursive($this->data, $data);
        
        return $this;
    }
}
