<?php

namespace Meccano\Database\Model;

class Article extends AbstractTranslableModel implements ModelInterface, TranslableModelInterface
{
    public function getTable()
    {
        return 'articles';
    }
}
