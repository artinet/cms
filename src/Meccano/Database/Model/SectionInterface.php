<?php
namespace Meccano\Database\Model;


interface SectionInterface
{
    /**
     * Get Seciton `id`
     * @return NULL|mixed
     */
    public function getId();
    
    /**
     * Get Section `lt`
     * @return NULL|integer
     */
    public function getLt();
    
    /**
     * Get Section `rt`
     * @return NULL|integer
     */
    public function getRt();
    
    /**
     * Get Section `level`
     * @return NULL|integer
     */
    public function getLevel();
    
    /**
     * Get Section `alias`
     * @return NULL|string
     */
    public function getAlias();
}
