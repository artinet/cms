<?php

namespace Meccano\Database\Model;

interface TranslableModelInterface
{
    public function getTableTranslate();
    public function setLocales($locales);
    //public function translate($name, $locale);
}
