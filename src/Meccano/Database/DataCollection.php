<?php

namespace Meccano\Database;

class DataCollection implements \Countable, \IteratorAggregate, \JsonSerializable
{
    private $data = array();

    public function __construct(array $data = array())
    {
        $this->data = $data;
    }

    /**
     * Count elements
     *
     * @return int
     */
    public function count()
    {
        return sizeof($this->data);
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    /**
     * Add an item
     *
     * @param $item mixed
     * @return $this
     */
    public function add($item)
    {
        $this->data[] = $item;

        return $this;
    }

    /**
     * Set an item
     *
     * @param $i int
     * @param $item mixed
     * @return $this
     */
    public function set($i, $item)
    {
        $this->data[$i] = $item;

        return $this;
    }

    /**
     * Get an item
     *
     * @param $i int
     * @return null
     */
    public function get($i)
    {
        return isset($this->data[$i]) ? $this->data[$i] : null;
    }

    /**
     * Remove an item by its id
     *
     * @param $i int
     * @return $this
     */
    public function remove($i)
    {
        unset($this->data[$i]);

        return $this;
    }

    /**
     * Remove an item
     *
     * @param $item int
     * @return DataCollection
     */
    public function removeByItem($item)
    {
        $i = array_search($item, $this->data, true);

        return $this->remove($i);
    }

    /**
     * Find an element
     *
     * @param \Closure $p
     * @return mixed
     */
    public function find(\Closure $p)
    {
        foreach ($this->data as $i => $item) {
            if ($p($i, $item)) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Get First Element of collection
     *
     * @inheritdoc
     */
    public function first()
    {
        return reset($this->data);
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
