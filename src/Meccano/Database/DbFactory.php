<?php

namespace Meccano\Database;

use Meccano\Config;

class DbFactory
{
    private static $dbConfig;

    public static function createDbService(Config $config)
    {
        self::$dbConfig = $config->get('database.connections.'. $config->get('database.defaultConnection'));

        $db = new Db(
            self::buildDSN(),
            self::$dbConfig['username'],
            self::$dbConfig['password'],
            self::$dbConfig['options']
        );
        $db->setTablePrefix(self::$dbConfig['table_prefix']);

        return $db;
    }

    private static function buildDSN()
    {
        return self::$dbConfig['adapter']           .':'.
            'dbname='.  self::$dbConfig['database'] .';'.
            'host='.    self::$dbConfig['hostname'] .';'.
            'charset='. self::$dbConfig['charset'];
    }
}
