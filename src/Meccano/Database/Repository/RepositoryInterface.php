<?php

namespace Meccano\Database\Repository;

use Meccano\Database\Db;
use Meccano\Database\Model\ModelInterface;

interface RepositoryInterface
{
    public function setModel(ModelInterface $model);
    public function setConnection(Db $db);
}
