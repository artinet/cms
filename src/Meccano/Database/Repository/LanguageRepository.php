<?php

namespace Meccano\Database\Repository;

use Meccano\Database\Model\Language;

class LanguageRepository extends ModelRepository
{
    private $defaultLanguage;

    /**
     * Get Default language
     *
     * @return Language|null
     */
    public function getDefaultLanguage()
    {
        if (!$this->defaultLanguage) {
            if (sizeof($this->allData) > 0) {
                $this->defaultLanguage = $this->allData->find(function ($i, Language $language) {
                    if ($language['is_default']) {
                        return true;
                    }

                    return false;
                });
            } else {
                $this->defaultLanguage = $this
                    ->where()
                    ->order('is_default', 'desc')
                    ->limit(1)
                    ->getOne();
            }
        }

        return $this->defaultLanguage;
    }
}
