<?php

namespace Meccano\Database\Repository;

use Meccano\Database\Condition\ConditionCollection;
use Meccano\Database\Condition\ConditionMather;
use Meccano\Database\DataCollection;
use Meccano\Database\Db;
use Meccano\Database\Model\ModelInterface;

class ModelRepository implements RepositoryInterface
{
    /** @var Db */
    protected $db;

    /** @var  ModelInterface */
    protected $model;

    /** @var ConditionCollection  */
    protected $conditions;

    /** @var DataCollection  */
    protected $allData;

    /** @var int|null */
    protected $rowsLimit;

    /** @var int */
    protected $rowsOffset;

    /** @var array */
    protected $rowsOrder = array();


    public function __construct()
    {
        $this->rowsOffset = 0;
        $this->allData    = new DataCollection();
        $this->conditions = new ConditionCollection();
    }


    /**
     * Set connection to DB
     *
     * @param Db $db
     * @return $this
     */
    public function setConnection(Db $db)
    {
        $this->db = $db;

        return $this;
    }

    /**
     * Set Model
     *
     * @param ModelInterface $model
     * @return $this
     */
    public function setModel(ModelInterface $model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Load all models in repository
     *
     * @param bool $force
     * @return DataCollection
     */
    public function all($force = false)
    {
        if ($force || count($this->allData) == 0) {
            $this->allData = $this->where()->get();
        }

        return $this->allData;
    }

    /**
     * @param string $sql
     * @param array $parameters
     * @return array
     */
    public function query($sql, $parameters)
    {
        return $this->db->query($sql, $parameters);
    }

    /**
     * @return DataCollection
     */
    public function get()
    {
        $dataCollection = new DataCollection();
        $sql            = 'SELECT * FROM ' . $this->tableName();
        $parameters     = array();

        // Apply Conditions
        $conditions = $this->conditions->getSql();
        if (!empty($conditions)) {
            $sql .= ' WHERE '. $conditions;
            $parameters = $this->conditions->getParameters();
        }

        // Apply Order
        if (!empty($this->rowsOrder)) {
            $sql .= ' ORDER BY '. join(', ', $this->rowsOrder);
        }

        // Apply Limit
        if ($this->rowsLimit) {
            $sql .= ' LIMIT '. $this->rowsLimit;
        }

        // Apply Offset
        if ($this->rowsOffset) {
            $sql .= ' OFFSET '. $this->rowsOffset;
        }

        $data = $this->db->query($sql, $parameters);

        if (sizeof($data) > 0) {
            foreach ($data as $item) {
                $itemObject = clone $this->model;
                $itemObject->fromArray($item);

                $dataCollection->add($itemObject);
            }
        }

        $this->reset();

        return $dataCollection;
    }

    /**
     * Get First Result of collection
     *
     * @return ModelInterface
     */
    public function getOne()
    {
        /** @var DataCollection $data */
        $data = $this
            ->limit(1)
            ->get();

        $this->reset();

        return $data->first();
    }

    /**
     * Start building conditions
     *
     * @param $field
     * @param $value
     * @param string $condition
     * @return $this|ModelRepository
     */
    public function where($field = null, $value = null, $condition = '=')
    {
        // Reset conditions
        $this->conditions = new ConditionCollection();

        if ($field && $value) {
            return $this->andWhere($field, $value, $condition);
        }

        return $this;
    }

    /**
     * Add AND condition
     *
     * @param $field
     * @param $value
     * @param string $condition
     * @return $this
     */
    public function andWhere($field, $value, $condition = '=')
    {
        $this->conditions->addAndCondition(ConditionMather::match($field, $value, $condition));

        return $this;
    }

    /**
     * Add OR condition
     *
     * @param $field
     * @param $value
     * @param string $condition
     * @return $this
     */
    public function orWhere($field, $value, $condition = '=')
    {
        $this->conditions->addOrCondition(ConditionMather::match($field, $value, $condition));

        return $this;
    }

    /**
     * Set SQL offset
     *
     * @param int $offset
     * @return $this
     */
    public function offset($offset)
    {
        $this->rowsOffset = (int)$offset;

        return $this;
    }

    /**
     * Set SQL rows limit
     *
     * @param $rows
     * @return $this
     */
    public function limit($rows)
    {
        $this->rowsLimit =(int)$rows;

        return $this;
    }

    /**
     * Reset Order parameters
     *
     * @param string $field
     * @param string $direction
     * @return $this|ModelRepository
     */
    public function order($field, $direction = 'asc')
    {
        $this->rowsOrder = array();

        return $this->addOrder($field, $direction);
    }

    /**
     * Add Order parameter
     *
     * @param string $field
     * @param string $direction
     * @return $this
     */
    public function addOrder($field, $direction = 'asc')
    {
        $this->rowsOrder[$field] = '`'. $field .'` '. (strtolower(trim($direction)) == 'desc' ? 'desc' : 'asc');

        return $this;
    }

    /**
     * Reset all conditions and modificators
     *
     * @return $this
     */
    public function reset()
    {
        $this->rowsLimit  = null;
        $this->rowsOffset = null;
        $this->rowsOrder  = array();
        $this->conditions = new ConditionCollection();
        $this->allData    = new DataCollection();

        return $this;
    }

    /**
     * @param $id
     * @return ModelInterface
     */
    public function find($id)
    {
        return $this
            ->where('id', $id)
            ->getOne();
    }

    /**
     * Get concatenated name of the table
     *
     * @return string
     */
    protected function tableName()
    {
        return $this->db->getTablePrefix() . $this->model->getTable();
    }
}
