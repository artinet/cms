<?php

namespace Meccano\Database\Condition;

interface ConditionInterface
{
    public function getSql();
    public function getParameters();
}
