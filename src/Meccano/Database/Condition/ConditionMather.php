<?php

namespace Meccano\Database\Condition;

class ConditionMather
{
    /**
     * @param $field
     * @param $value
     * @param $condition
     * @return Condition
     */
    public static function match($field, $value, $condition)
    {
        // TODO: implement matcher itself

        switch ($condition) {
            default:
                $conditionClass = new Condition();
        }

        $conditionClass
            ->setField($field)
            ->setValue($value)
            ->setCondition($condition);

        return $conditionClass;
    }
}
