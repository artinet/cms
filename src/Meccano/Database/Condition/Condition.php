<?php

namespace Meccano\Database\Condition;

class Condition implements ConditionInterface, ConditionSetterInterface
{
    /** @var mixed */
    protected $value;

    /** @var string */
    protected $field;

    /** @var string */
    protected $condition;

    /**
     * Set value
     *
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Set field
     *
     * @param string $field
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Set condition between field and value
     *
     * @param string $condition
     * @return $this
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * Get Parameters for SQL placeholders
     *
     * @return array
     */
    public function getParameters()
    {
        return array($this->value);
    }

    /**
     * Get SQL string with placeholder
     *
     * @return string
     */
    public function getSql()
    {
        return '`'. $this->field .'` '. $this->condition .' ?';
    }
}
