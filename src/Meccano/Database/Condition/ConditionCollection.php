<?php

namespace Meccano\Database\Condition;

class ConditionCollection implements ConditionInterface, ConditionCollectionInterface, \Countable
{
    /** @var bool */
    private $built = false;

    /** @var array  */
    private $conditions = array();

    /** @var string */
    private $sql;

    /**
     * @var array
     */
    private $parameters = array();

    /**
     * @return int
     */
    public function count()
    {
        return sizeof($this->conditions);
    }

    /**
     * Add AND condition
     *
     * @param ConditionInterface $condition
     * @return $this
     */
    public function addAndCondition(ConditionInterface $condition)
    {
        $this->unsetBuild();
        $this->addCondition('and', $condition);

        return $this;
    }

    /**
     * Add OR condition
     *
     * @param ConditionInterface $condition
     * @return $this
     */
    public function addOrCondition(ConditionInterface $condition)
    {
        $this->unsetBuild();
        $this->addCondition('or', $condition);

        return $this;
    }

    /**
     * Add condition
     *
     * @param string $join
     * @param ConditionInterface $condition
     */
    private function addCondition($join, ConditionInterface $condition)
    {
        $this->conditions[] = array(
            'join' => $join,
            'condition' => $condition
        );
    }

    /**
     * Unset built status
     */
    private function unsetBuild()
    {
        $this->built    = false;
        $this->sql        = '';
        $this->parameters = array();
    }

    /**
     * Build Conditions
     */
    private function build()
    {
        if (!$this->built) {
            if (sizeof($this->conditions) > 0) {
                $sql        = '';
                $parameters = array();

                /** @var ConditionInterface $el */
                foreach ($this->conditions as $el) {
                    $sql .= (strlen($sql) > 0 ? ' '. $el['join'] .' ' : '');
                    if ($el['condition'] instanceof ConditionCollectionInterface) {
                        $sql .= '('. $el['condition']->getSql() .')';
                    } else {
                        $sql .= $el['condition']->getSql();
                    }
                    $parameters = array_merge($parameters, $el['condition']->getParameters());
                }

                $this->sql        = $sql;
                $this->parameters = $parameters;
            }

            $this->built = true;
        }
    }

    /**
     * Get string of SQL conditions
     *
     * @return string
     */
    public function getSql()
    {
        $this->build();
        return $this->sql;
    }

    /**
     * Get array of parameters
     *
     * @return array
     */
    public function getParameters()
    {
        $this->build();
        return $this->parameters;
    }
}
