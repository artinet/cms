<?php

namespace Meccano\Database\Condition;

interface ConditionSetterInterface
{
    public function setField($field);
    public function setValue($value);
    public function setCondition($condition);
}
