<?php

/**
 * Default Dependencies for the project
 *
 * PHP version 5.5
 *
 * @category Bootstrap
 * @package  MeccanoCMS
 * @author   Novachok Oleksandr <novachok@gmail.com>
 * @license  MIT (https://opensource.org/licenses/MIT)
 * @link     http://artinet.com.ua
 */

use Symfony\Component\DependencyInjection;

$container = new DependencyInjection\ContainerBuilder();

$container->register('config', 'Meccano\Config')
    ->setArguments(array(realpath(__DIR__ .'/../config')));


return $container;
